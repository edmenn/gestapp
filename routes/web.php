<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProyectoController;
use App\Http\Controllers\SubProyectoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\DepartamentoController;
use App\Http\Controllers\DepProyectoController;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\PresupuestoController;
use App\Http\Controllers\PagoController;
use App\Http\Controllers\PuertoController;
use App\Http\Controllers\AuditoriaController;

use Spatie\Permission\Traits\HasRoles;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProyectoController::class, 'index'])->middleware(['auth'])->name('index');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');


Route::middleware(['auth'])->group(function () {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::get('proyectos/reporte/ver', [ProyectoController::class, 'reporteVer'])->name('proyectos.reporte.ver');
    Route::get('proyectos/reporte/descargar', [ProyectoController::class, 'reporteDescargar'])->name('proyectos.reporte.descargar');
    Route::resource('proyectos', ProyectoController::class);
    Route::resource('departamentos', DepartamentoController::class);
    Route::resource('proveedores', ProveedorController::class);
    Route::resource('puertos', PuertoController::class);
    Route::resource('proyecto.sub_proyectos', SubProyectoController::class);
    Route::resource('departamento.proyectos', DepProyectoController::class);
    Route::resource('sub_proyecto.presupuestos', PresupuestoController::class);
    Route::resource('presupuesto.pagos', PagoController::class);
    Route::get('presupuesto/{presupuesto}/pagos/{pago}/download', [PagoController::class, 'download'])->name('presupuesto.pagos.download');
    Route::get('auditorias', [AuditoriaController::class, 'index'])->name('auditorias');
});

require __DIR__.'/auth.php';
