@extends('app')

@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 1.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <div class="row card shadow p-3 m-3 mt-5">
                <div class="titulo col-lg-12 margin-tb">
                    <div class="float-start">
                        <h2>Añadir un nuevo SubProyecto</h2>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-primary" href="{{ route('proyectos.show', [$proyecto]) }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                    </div>
                </div>
            {{-- </div> --}}
            {{-- <div class="row"> --}}
                <div class="col-lg-12 margin-tb">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hubieron algunos problemas.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('proyecto.sub_proyectos.update', ['proyecto' => $proyecto, 'sub_proyecto' => $subProyecto]) }}" method="POST" >
                        @csrf
                        @method('PATCH')
                
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input type="text" name="nombre" value="{{ $subProyecto->nombre }}" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Descripcion:</strong>
                                    <textarea class="form-control" style="height:50px" name="descripcion"
                                        placeholder="Descripcion">{{ $subProyecto->descripcion }}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Costo:</strong>
                                    <input class="form-control" type="number" id="costo" name="costo" value="{{ $subProyecto->costo }}">
                                    <small>Saldo disponible es: {{ $saldo_disponible }} </small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
