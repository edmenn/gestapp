@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
        a {
            color: #212529
        }
    </style>
@endsection
@section('content')
    <div class="card mb-3 mt-4 shadow">
        <div class="card-header">
            <h1 class="card-title text-center">{{ $subProyecto->nombre }}</h1>
        </div>
        <div class="card-body">
            <div class="row pl-3 pr-3">
                <div class="col-sm-6 mt-2">
                    <a href=" {{ route('proyectos.show', [$proyecto]) }} ">
                        <div class="card shadow">
                            <p class="text-center mt-2 mb-2"><strong>Proyecto: </strong>{{ $proyecto->nombre }}</p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Fecha de creación: </strong>{{ date_format($subProyecto->created_at, 'd/m/Y') }}</p>
                    </div>
                </div>
                <div class="col-sm-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Costo: </strong>{{ number_format($subProyecto->costo, 0, ',', '.') }}  PYG</p>
                    </div>
                </div>
                <div class="col-sm-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Contratado: </strong>{{ number_format($subProyecto->contratado, 0, ',', '.') }} PYG</p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card shadow mt-2">
                        <p class="text-center mt-2 mb-2"><strong>Descripción: </strong>{{ $subProyecto->descripcion }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="d-flex justify-content-center">
                {{-- @if ($subProyecto->presupuesto && $subProyecto->presupuesto->pago)
                    <div class="justify-content-center">
                        <a class="btn btn-info" href="{{ route('presupuesto.pagos.show', [$subProyecto->presupuesto->id, $subProyecto->presupuesto->pago->id]) }}">
                            Ver Pago 
                        </a>
                    </div> --}}
                @if ($subProyecto->presupuesto)
                    @if ($subProyecto->contratado < $subProyecto->presupuesto->monto )
                        <div class="justify-content-center">
                            <a class="btn btn-primary" href="{{ route('presupuesto.pagos.create', [$subProyecto->presupuesto->id]) }}">
                                Adjuntar Pago 
                            </a>
                        </div>
                    @endif
                @else
                    <div class="justify-content-center">
                        <a class="btn btn-primary" href="{{ route('sub_proyecto.presupuestos.create', [$subProyecto]) }}">
                            Agregar Presupuesto
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @if ($subProyecto->presupuesto)
        <div class="titulo row">
            <div class="mt-7 col-12">
                <div class="float-start">
                    <h3>Presupuesto de {{ $subProyecto->nombre }}</h3>
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                {{ $message }}
            </div>
        @endif

        <div class="shadow p-3 mb-5 bg-white rounded card-container table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">Concepto</th>
                    <th scope="col">Proveedor</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Cotizacion del dia</th>
                    <th scope="col">Monto USD</th>
                    <th scope="col">Fecha Creación</th>
                    <th style="width: 10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        {{-- <td>{{ $subProyecto->presupuesto }}</td> --}}
                        <td>{{ $subProyecto->presupuesto->concepto }}</td>
                        <td>{{ $subProyecto->presupuesto->proveedor->nombre_fantasia }}</td>
                        <td style="text-align:right">{{ number_format($subProyecto->presupuesto->monto, 0, ',', '.') }}</td>
                        <td style="text-align:right">{{ number_format($subProyecto->presupuesto->cotizacion_dia, 2, ',' , '.') }}</td>
                        <td style="text-align:right">{{ number_format($subProyecto->presupuesto->monto_usd, 2, ',', '.') }}</td>
                        <td>{{ date_format($subProyecto->created_at, 'd/m/Y') }}</td>
                        <td style="width: 10%">
                            <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                <a href="{{ route('sub_proyecto.presupuestos.edit', [$subProyecto, $subProyecto->presupuesto]) }}" class="btn btn-outline-primary btn-sm">Ver</a>
                                <a href="{{ route('sub_proyecto.presupuestos.edit', [$subProyecto, $subProyecto->presupuesto]) }}" class="btn btn-outline-primary btn-sm">Editar</a>
                                <form action="{{ route('sub_proyecto.presupuestos.destroy', [$subProyecto, $subProyecto->presupuesto]) }}" method="POST" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    
                                    <button type="submit" title="delete" class="btn btn-outline-danger btn-sm">
                                        Eliminar
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif
    
    @if ($subProyecto->presupuesto)
        @if ($subProyecto->presupuesto->pagos->count() > 0)
            <div class="titulo row">
                <div class="mt-7 col-12">
                    <div class="float-start">
                        <h3>Pagos de  {{ $subProyecto->nombre }}</h3>
                    </div>
                </div>
            </div>

            <div class="shadow p-3 mb-5 bg-white rounded card-container table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">Concepto</th>
                        <th scope="col">Comprobante</th>
                        <th scope="col">Monto</th>
                        <th scope="col">Fecha de Pago</th>
                        <th scope="col">Cotizacion del dia</th>
                        <th scope="col">Monto USD</th>
                        <th scope="col">Monto IVA</th>
                        <th style="width: 10%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($subProyecto->presupuesto->pagos as $pago)
                            <tr>
                                <td>{{ $pago->concepto }}</td>
                                <td>{{ $pago->file }}</td>
                                <td style="text-align:right">{{ number_format($pago->monto, 0, ',', '.') }}</td>
                                <td>{{ date_format($pago->created_at, 'd/m/Y') }}</td>
                                <td style="text-align:right">{{ number_format($pago->cotizacion_dia, 2, ',', '.') }}</td>
                                <td style="text-align:right">{{ number_format($pago->monto_usd, 2, ',', '.') }}</td>
                                <td style="text-align:right">{{ number_format($pago->monto_iva, 0, ',', '.') }}</td>
                                <td style="width: 10%">
                                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                        <a href="{{ route('presupuesto.pagos.show', [$subProyecto->presupuesto, $pago]) }}" class="btn btn-outline-primary btn-sm">Ver</a>
                                        @if (Auth::user()->hasRole('Admin'))
                                            <a href="{{ route('presupuesto.pagos.edit', [$subProyecto->presupuesto, $pago]) }}" class="btn btn-outline-primary btn-sm">Editar</a>
                                            <form action="{{ route('presupuesto.pagos.destroy', [$subProyecto->presupuesto, $pago]) }}" method="POST" style="display: inline-block">
                                                @csrf
                                                @method('DELETE')
                                                
                                                <button type="submit" title="delete" class="btn btn-outline-danger btn-sm">
                                                    Eliminar
                                                </button>
                                            </form>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    @endif
@endsection
@section('scripts')
<script>
    montos_elemeentos = document.getElementsByClassName('monto');
    montos_elemeentos.forEach(element => {
        monto_string = element.innerHTML
        let monto = parseFloat(elementContent);
        element.innerHTML = new Intl.NumberFormat('es-PY').format(monto);
    });
</script>
@endsection