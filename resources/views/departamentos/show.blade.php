@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="text-center">
                <h1>Departamento de {{ $departamento->nombre }}</h1>
            </div>
        </div>
    </div>
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h2>Proyectos</h2>
            </div>
            @if (Auth::user()->hasRole('Admin'))
                <div class="float-end">
                    <a class="btn btn-primary" href="{{ route('departamento.proyectos.create', [$departamento]) }}">
                        Agregar Proyecto
                    </a>
                </div>
            @endif
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="shadow p-3 mb-5 bg-white rounded card-container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Departamento</th>
                <th scope="col">Puerto</th>
                <th scope="col">Responsable</th>
                <th scope="col">Estado</th>
                <th scope="col">Costo</th>
                <th scope="col">Cotizacion del dia</th>
                <th scope="col">Monto USD</th>
                <th scope="col">Contratado</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($departamento->proyectos as $proyecto)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $proyecto->nombre }}</td>
                    <td>{{ $proyecto->departamento->nombre }}</td>
                    <td>{{ $proyecto->puerto->nombre }}</td>
                    <td>{{ $proyecto->user->name }}</td>
                    <td>{{ $proyecto->estado->nombre }}</td>
                    <td>{{ number_format($proyecto->costo, 0, ',', '.') }}</td>
                    <td>{{ number_format($proyecto->cotizacion_dia, 2, ',', '.') }}</td>
                    <td>{{ number_format($proyecto->monto_usd, 2, ',', '.') }}</td>
                    <td>{{ number_format($proyecto->contratado, 0, ',', '.') }}</td>
                    {{-- <td>{{ date_format($proyecto->created_at, 'jS M Y') }}</td> --}}
                    <td style="width: 10%">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="{{ route('proyectos.show', [$proyecto]) }}" class="btn btn-outline-primary">Ver</a>
                            @if (Auth::user()->hasRole('Admin'))
                            <a href="{{ route('proyectos.edit', [$proyecto]) }}" class="btn btn-outline-primary">Editar</a>
                            <form action="{{ route('proyectos.destroy', [$proyecto]) }}" method="POST" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                
                                <button type="submit" title="delete" class="btn btn-outline-danger">
                                    Eliminar
                                </button>
                            </form>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>

    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h2>Usuarios</h2>
            </div>
        </div>
    </div>

    <div class="shadow p-3 mb-5 bg-white rounded card-container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($departamento->usuarios as $user)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                      @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $role)
                          {{ $role }}
                        @endforeach
                      @endif
                    </td>
                    <td style="width: 10%">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="{{ route('users.show', $user->id) }}" class="btn btn-outline-primary">Ver</a>
                            @if (Auth::user()->hasRole('Admin'))
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-primary">Editar</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger']) !!}
                            {!! Form::close() !!}
                            @endif
                        </div>
                    </td>
                </tr>
              @endforeach
            </tbody>
          </table>
      </div>

      <div class="row d-flex justify-content-center">
        <div class="col-12">
            <!-- Chart's container -->
        <h2 class="text-center">Proyectos del Departamento de {{ $departamento->nombre }}</h2>
        <div id="chart" style="height: 300px;"></div>
            <!-- Charting library -->
            <script src="https://unpkg.com/echarts/dist/echarts.min.js"></script>
            <!-- Chartisan -->
            <script src="https://unpkg.com/@chartisan/echarts/dist/chartisan_echarts.js"></script>
            <!-- Your application script -->
            <script>
                const chart1 = new Chartisan({
                    el: '#chart',
                    url: "@chart('proyectos_dep_chart')"+"?id={{ $departamento->id }}",
                    hooks: new ChartisanHooks()
                    .colors(['#4299E1','#FE0045','#C07EF1','#67C560','#ECC94B'])
                        .datasets('bar')
                        .axis(true)
                });
            </script>
        </div>
    </div>

    </div>

@endsection

@section('scripts')
{!! $chart->renderChartJsLibrary() !!}
{!! $chart->renderJs() !!}
@endsection