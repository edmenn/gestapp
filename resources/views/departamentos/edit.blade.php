@extends('app')


@section('content')
<div class="row d-flex justify-content-center"">
    <div class="col-lg-8 margin-tb">
        <div class="row card shadow p-3 m-3 mt-5">
            <div class="titulo col-lg-12 mt-2 mb-2">
                <div class="float-start">
                    <h2>Editar Usuario</h2>
                </div>
                <div class="float-end">
                    <a class="btn btn-primary" href="{{ route('departamentos.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                </div>
            </div>

            <div class="col-lg-12 margin-tb">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Hubieron unos problemas.<br><br>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                @endif

                {!! Form::model($departamento, ['method' => 'PATCH','route' => ['departamentos.update', $departamento->id]]) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {!! Form::text('name', $departamento->nombre, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Usuarios:</strong>
                            <br>
                            @foreach($usuarios as $user)
                                <label>{{ Form::checkbox('usuarios[]', $user->id, in_array($user->id, $usuariosDepartamento) ? true : false, array('class' => 'name')) }}
                                    {{ $user->name }}</label>
                            <br/>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-2">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
