@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')

  <div class="titulo row">
    <div class="mt-7 col-12 mb-2">
        <div class="text-center">
            <h3>Auditoria</h3>
        </div>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
  @endif

  <div class="shadow p-3 mb-5 bg-white rounded card-container">
    <table class="table">
        <thead>
            <tr class="text-center">
                <th scope="col">Usuario</th>
                <th scope="col">Tabla</th>
                <th scope="col">Id</th>
                <th scope="col">Accion</th>
                <th scope="col">Fecha</th>
              </tr>
        </thead>
        <tbody>
            @foreach ($auditorias as $auditoria)
                <tr class="text-center">
                    <td>{{ $auditoria->user->name }}</td>
                    <td>{{ $auditoria->table }}</td>
                    <td>{{ $auditoria->model_id }}</td>
                    <td>{{ $auditoria->action }}</td>
                    <td>{{ $auditoria->created_at }}</td>
                    <td>
                    </td>
                </tr>
            @endforeach
            </tbody>
      </table>
  </div>


@endsection
