@extends('app')

@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 1.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <div class="row card shadow p-3 m-3 mt-5">
                <div class="titulo col-lg-12 margin-tb">
                    <div class="float-start">
                        <h2>Añadir Pago</h2>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-primary" href="{{ route('proyecto.sub_proyectos.show', [$presupuesto->subproyecto->proyecto->id, $presupuesto->subproyecto->id]) }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                    </div>
                </div>
            {{-- </div> --}}
            {{-- <div class="row"> --}}
                <div class="col-lg-12 margin-tb">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hubo algunos problemas.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('presupuesto.pagos.store', $presupuesto->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Concepto:</strong>
                                    <input type="text" name="concepto" class="form-control" placeholder="concepto">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Monto:</strong>
                                    <input class="form-control" type="number" id="monto" name="monto">
                                    <small>Monto total que falta pagar es: {{ $presupuesto->monto - $presupuesto->subproyecto->contratado }} PYG</small>
                                    <br>
                                    <small>Dolar compra el dia de hoy es: {{ $dolarCompra }} </small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Cargar archivo <small>(Archivos permitidos: WORD, PDF, EXCEL, JPG, PNG)</small></strong>
                                    <input id="file" type="file" class="form-control" name="file">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
$("monto").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
})
</script>
@endsection