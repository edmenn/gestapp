@extends('app')
@section('styles')
    <style>
        a { color: #212529; }
    </style>
@endsection
@section('content')
    <div class="card mb-3 mt-4 shadow">
        <div class="card-header">
            <h1 class="card-title text-center">Pago del {{ $presupuesto->concepto }}</h1>
        </div>
        <div class="card-body">
            <div class="row pl-3 pr-3">
                <div class="col-12 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Concepto: </strong>{{ $pago->concepto }}</p>
                    </div>
                </div>
                <div class="col-6 mt-2">
                    <a href="{{ route('proyecto.sub_proyectos.show', [$presupuesto->subproyecto->proyecto->id, $presupuesto->subproyecto->id]) }}">
                        <div class="card shadow">
                            <p class="text-center mt-2 mb-2"><strong>Presupuesto: </strong>{{ $presupuesto->concepto }}</p>
                        </div>
                    </a>
                </div>
                <div class="col-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Monto: </strong>{{ $pago->monto }}</p>
                    </div>
                </div>
                <div class="col-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Monto IVA: </strong>{{ $pago->monto_iva }}</p>
                    </div>
                </div>
                <div class="col-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Fecha de creación: </strong>{{ date_format($pago->created_at, 'd/m/Y') }}</p>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2">
                            <strong>Enlace al archivo: 
                                <a href="{{ asset('storage/files/'.$pago->file) }}" title="Ver Archivo" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('presupuesto.pagos.download', [$presupuesto->id, $pago->id]) }}" title="Descargar Archivo" class="btn btn-info"><i class="fa fa-download"></i></a>
                            </strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="d-flex justify-content-center">
                @if (Auth::user()->hasRole('Admin'))
                    <div class="justify-content-center">
                        <form action="{{ route('presupuesto.pagos.destroy', [$presupuesto->id, $pago->id]) }}" method="POST" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" title="delete" class="btn btn-outline-danger">
                                Eliminar
                            </button>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

@endsection