@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
        td{
            text-align:center;
        }
    </style>
@endsection
@section('content')
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h2>Proyectos</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-info" href="{{ route('proyectos.reporte.ver') }}" target="_blank">Ver Reporte <i class="fa fa-eye"></i></a>
                <a class="btn btn-primary" href="{{ route('proyectos.reporte.descargar') }}">Descargar Reporte <i class="fa fa-download"></i></a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="shadow p-3 mb-5 bg-white rounded card-container table-responsive">
        <table class="table table-striped table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Departamento</th>
                <th scope="col">Puerto</th>
                <th scope="col">Responsable</th>
                <th scope="col">Estado</th>
                <th scope="col">Costo</th>
                <th scope="col">Cotizacion del dia</th>
                <th scope="col">Monto USD</th>
                <th scope="col">Contratado</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($proyectos as $proyecto)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $proyecto->nombre }}</td>
                    <td>{{ $proyecto->departamento->nombre }}</td>
                    <td>{{ $proyecto->puerto->nombre }}</td>
                    <td>{{ $proyecto->user->name }}</td>
                    <td style="text-align:center">{{ $proyecto->estado->nombre }}</td>
                    <td style="text-align:right">{{ number_format($proyecto->costo, 0, ',', '.') }}</td>
                    <td style="text-align:right">{{ number_format($proyecto->cotizacion_dia, 2, ',', '.') }}</td>
                    <td style="text-align:right">{{ number_format($proyecto->monto_usd, 2, ',', '.') }}</td>
                    <td style="text-align:right">{{ number_format($proyecto->contratado, 0, ',', '.') }}</td>
                   {{-- <td>{{ date_format($proyecto->created_at, 'jS M Y') }}</td> --}}
                    <td style="width: 10%">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="{{ route('proyectos.show', [$proyecto]) }}" class="btn btn-outline-primary">Ver</a>
                            @if (Auth::user()->hasRole('Admin'))
                                <a href="{{ route('proyectos.edit', [$proyecto]) }}" class="btn btn-outline-primary">Editar</a>
                                <form action="{{ route('proyectos.destroy', [$proyecto]) }}" method="POST" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    
                                    <button type="submit" title="delete" class="btn btn-outline-danger">
                                        Eliminar
                                    </button>
                                </form>
                            @endif
                          </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-12">
            <!-- Chart's container -->
        <h2 class="text-center">Cantidad de Proyectos por Estado</h2>
        <div id="chart1" style="height: 300px;"></div>
            <!-- Charting library -->
            <script src="https://unpkg.com/echarts/dist/echarts.min.js"></script>
            <!-- Chartisan -->
            <script src="https://unpkg.com/@chartisan/echarts/dist/chartisan_echarts.js"></script>
            <!-- Your application script -->
            <script>
                const chart1 = new Chartisan({
                    el: '#chart1',
                    url: "@chart('my_chart')",
                    hooks: new ChartisanHooks()
                    .colors(['#4299E1','#FE0045','#C07EF1','#67C560','#ECC94B'])
                        .datasets('bar')
                        .axis(true)
                });
            </script>
        </div>
        <div class="col-10 justify-content-center align-items-center">
            <hr>
        </div>
        <div class="col-12">
            <!-- Chart's container -->
        <h2 class="text-center">Cantidad de Proyectos por Departamento</h2>
        <div id="chart2" style="height: 300px;"></div>
            <!-- Your application script -->
            <script>
                const chart2 = new Chartisan({
                    el: '#chart2',
                    url: "@chart('dep_proyecto_chart')",
                    hooks: new ChartisanHooks()
                    .colors(['#4299E1','#FE0045','#C07EF1','#67C560','#ECC94B'])
                        .datasets('bar')
                        .axis(true)
                });
            </script>
        </div>
    </div>

@endsection
@section('scripts')

@endsection