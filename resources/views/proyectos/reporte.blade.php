<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reporte Proyectos</title>
</head>
<style type="text/css">
    table{
        font-family: arial,sans-serif;
        border-collapse:collapse;
        width:100%;
        font-size:11px;
    }
    td, th{
        border:1px solid #dddddd;
        text-align: left;
        padding:8px;        
    }
    tr:nth-child(even){
        background-color:#dddddd;        
    }
    
    th {
       text-align: center;
    }

    h2{
        text-align: center;
        font-size:14px;
        margin-bottom:5px;
    }
    h3{
        text-align: left;
        font-size:12px;
        margin-bottom:5px;
    }
</style>  
    <body class="font-sans antialiased">
        <div class="titulo row">
            <div class="mt-7 col-12">
                <div class="float-start">
                    <h2>Proyectos</h2>
                </div>
            </div>
        </div>

        <div class="shadow p-3 mb-5 bg-white rounded card-container">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Departamento</th>
                    <th scope="col">Puerto</th>
                    <th scope="col">Responsable</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Cotizacion del dia</th>
                    <th scope="col">Monto USD</th>
                    <th scope="col">Contratado</th>
                </tr>
                </thead>
                <tbody>
                @php $i = 0; @endphp
                @foreach ($proyectos as $proyecto)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $proyecto->nombre }}</td>
                        <td>{{ $proyecto->departamento->nombre }}</td>
                        <td>{{ $proyecto->puerto->nombre }}</td>
                        <td>{{ $proyecto->user->name }}</td>
                        <td>{{ $proyecto->estado->nombre }}</td>
                        <td>{{ number_format($proyecto->costo, 0, ',', '.') }}</td>
                        <td>{{ number_format($proyecto->cotizacion_dia, 2, ',', '.') }}</td>
                        <td>{{ number_format($proyecto->monto_usd, 2, ',', '.') }}</td>
                        <td>{{ number_format($proyecto->contratado, 0, ',', '.') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>