@extends('app')


@section('content')
<div class="row d-flex justify-content-center">
    <div class="col-lg-10 mt-5 mb-2">
        <div class="float-start ml-7">
            @if(Auth::user()->hasRole('Admin'))
                <a class="btn btn-primary" href="{{ route('users.index') }}" title="Atras"> <i class="fas fa-backward "></i> </a>
            @else
                <a class="btn btn-primary" onclick="history.back()"> <i class="fas fa-backward "></i></a>
            @endif
        </div>
    </div>
    <div class="col-lg-12 mb-2">
        <div class="text-center">
            <h2>Detalles del Usuario</h2>
        </div>
    </div>
</div>

<div class="bg-default-2 pt-28 pt-lg-30 pb-13 pb-xxl-32 pd-28 mb-28">
    <div class="container">
        <div class="d-flex row justify-content-center pt-1 pb-4 mb-4">
            {{-- <div class="col-12 col-xxl-7 col-lg-7 col-md-7 mb-0 mb-lg-1 mb-md-1 mt-0">
                <hr>
            </div> --}}
            <!-- Left Sidebar Start -->
            <div class="col-12 col-xxl-6 col-lg-5 col-md-6 mb-11 mb-lg-0 pt-4 shadow">
                <div class="pl-lg-5">
                <!-- Top Start -->
                <div class="bg-white shadow-9 rounded-4">
                    <div class="px-5 py-11 text-center border-bottom border-mercury">
                        <a class="mb-10" href="#">
                            <img class="align-self-center rounded" style="object-fit:contain; padding:0px; max-height: 200px; min-width: 150px; border-radius: 10%;" src="{{ asset('logo/avatar-profile.svg') }}" alt="default-pic">
                        </a>
                        {{-- <h4 class="mt-2 mb-0"><a class="text-black-2 font-size-6 font-weight-semibold" href="#">{{ $user->name }}</a></h4> --}}
                        </div>
                        <!-- Top End -->
                        <!-- Bottom Start -->
                        <div class="px-9 pt-lg-5 pt-9 pt-xl-9 pb-4">
                            <h5 class="text-center text-black-2 mb-8 font-size-1">Info de contacto</h5>
                            <!-- Single List -->
                            <div class="row d-flex justify-content-center mt-4 mb-2">
                                <div class="col-4 d-flex justify-content-center">Nombre</div>
                                <div class="col-6 d-flex justify-content-center"><input class="form-control" type="text" value="{{ $user->name }}" aria-label="readonly input example" readonly></div>
                                <div class="col-2"></div>
                            </div>
                            <div class="row d-flex justify-content-center mt-2 mb-2">
                                <div class="col-4 d-flex justify-content-center">Correo</div>
                                <div class="col-6 d-flex justify-content-center"><input class="form-control" type="text" value="{{ $user->email }}" aria-label="readonly input example" readonly></div>
                                <div class="col-2"></div>
                            </div>
                            <div class="row d-flex justify-content-center mt-2 mb-2">
                                <div class="col-4 d-flex justify-content-center">Roles</div>
                                <div class="col-8"></div>
                                <div class="col-4"></div>
                                <div class="col-8">
                                    @if(!empty($user->getRoleNames()))
                                        <ul>
                                            @foreach($user->getRoleNames() as $role)
                                                {{-- <div class="col-4"></div> --}}
                                                <li>{{ $role }}</li>
                                                {{-- <div class="col-2"></div> --}}
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            @if(Auth::user()->id==$user->id ||  Auth::user()->hasRole('Admin'))
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
                                    <a href="{{ route('users.edit', Auth::user()->id) }}" class="btn btn-primary">Editar</a>
                                </div>
                            @endif
                            <!-- Single List -->
                        </div>
                        <!-- Bottom End -->
                    </div>
                </div>
            </div>
            <!-- Left Sidebar End -->
        </div>
    </div>

    <div class="titulo row">
        <div class="mt-5 col-12">
            <div class="float-start">
                <h3>Registro de actividades</h3>
            </div>
        </div>
    </div>
    @if(Auth::user()->id==$user->id ||  Auth::user()->hasRole('Admin'))
    <div class="shadow p-3 mb-5 bg-white rounded card-container table-responsive">
        <table class="table">
            <thead>
              <tr class="text-center">
                <th scope="col">Tabla</th>
                <th scope="col">Id</th>
                <th scope="col">Accion</th>
                <th scope="col">Fecha</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($auditorias as $auditoria)
                <tr>
                    <td>{{ $auditoria->table }}</td>
                    <td>{{ $auditoria->model_id }}</td>
                    <td>{{ $auditoria->action }}</td>
                    <td>{{ $auditoria->created_at }}</td>
                    {{-- <td>{{ date_format($auditoria->created_at, 'd/m/Y') }}</td> --}}
                    <td>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endif
    
</div>

@endsection