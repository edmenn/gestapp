@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')

  <div class="titulo row">
    <div class="mt-7 col-12">
        <div class="float-start">
            <h3>Administración de Usuarios</h3>
        </div>
        @if (Auth::user()->hasRole('Admin'))
          <div class="float-end">
              <a class="btn btn-primary" href="{{ route('users.create') }}">
                  Agregar Usuario
              </a>
          </div>
        @endif
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
  @endif

  <div class="shadow p-3 mb-5 bg-white rounded card-container table-responsive">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Departamento</th>
            <th scope="col">Roles</th>
            @if (Auth::user()->hasRole('Admin'))
              <th style="width: 10%">Actions</th>
            @endif
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $key => $user)
            <tr>
                <th scope="row">{{ $i++ }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                @if( $user->departamento )
                  <td>{{ $user->departamento->nombre }}</td>
                @else
                  <td>Sin departamento</td>
                @endif
                <td>
                  @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $role)
                      {{ $role }}
                    @endforeach
                  @endif
                </td>
                @if (Auth::user()->hasRole('Admin'))
                <td style="width: 10%">
                  <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                      <a href="{{ route('users.show', $user->id) }}" class="btn btn-outline-primary">Ver</a>
                      <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-primary">Editar</a>
                      {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                          {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger']) !!}
                      {!! Form::close() !!}
                    </div>
                </td>
                @endif
            </tr>
          @endforeach
        </tbody>
      </table>
  </div>

  {!! $data->render() !!}

@endsection
