<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <style>
        .bg-navbar{
            background-color: #292961;
        }
        .bg-sidebar{
            background-color: #F0F0F0;
        }
        .letra-sidebar{
            color: #303030;
        }
        a{
            text-decoration: none;
            color: #212529;
        }
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
    </style>
    @yield('styles')
</head>
<body>
    @include('sidebar-menu')
    <nav class="navbar navbar-dark bg-navbar">
        <div class="container-fluid">
            <div class="justify-content-start">
                <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas" role="button" aria-controls="offcanvasWithBothOptions">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="dropdown d-flex" style="margin-right: 2.5rem">
                <a href="#" class="d-flex align-items-center link-light text-decoration-none dropdown-toggle" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
                {{-- <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2"> --}}
                <strong>{{ Auth::user()->name }}</strong>
                {{-- <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2"> --}}
                </a>
                {{-- <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2" style="margin-left: 0.5rem"> --}}
                <img src="{{ asset('logo/avatar-profile.svg') }}" alt="" width="32" height="32" class="rounded-circle me-2" style="margin-left: 0.5rem">
                <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2" style="">
                    {{-- <li><a class="dropdown-item" href="#">New project...</a></li> --}}
                    <li><a class="dropdown-item" href="{{ route('users.show', Auth::user()->id) }}">Perfil</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li>
                        {{-- <a class="dropdown-item" href="">Sign out</a> --}}
                        <!-- Authentication -->
                        {{-- <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-responsive-nav-link :href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                <li class="dropdown-item">{{ ('Salir') }}</li>
                            </x-responsive-nav-link>
                        </form> --}}
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            
                            <button type="submit" title="delete" class="dropdown-item" style="border: none; background-color:transparent;">
                                Salir
                            </button>
                        </form>
                    </li>
                </ul>
                {{-- <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2"> --}}
            </div>
        </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>
    @yield('scripts')
</body>
</html>