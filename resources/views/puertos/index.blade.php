@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h2>Puertos</h2>
            </div>
            @if (Auth::user()->hasRole('Admin'))
                <div class="float-end">
                    <a class="btn btn-primary" href="{{ route('puertos.create') }}">
                        Agregar Puerto
                    </a>
                </div>
            @endif
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="shadow p-3 mb-5 bg-white rounded card-container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Direccion</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($puertos as $puerto)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $puerto->nombre }}</td>
                    <td>{{ $puerto->direccion }}</td>
                    <td style="width: 10%">
                        @if (Auth::user()->hasRole('Admin'))
                            <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                <a href="{{ route('puertos.edit', [$puerto]) }}" class="btn btn-outline-primary">Ver</a>
                                <a href="{{ route('puertos.edit', [$puerto]) }}" class="btn btn-outline-primary">Editar</a>
                                <form action="{{ route('puertos.destroy', [$puerto]) }}" method="POST" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    
                                    <button type="submit" title="delete" class="btn btn-outline-danger">
                                        Eliminar
                                    </button>
                                </form>
                            </div>
                          @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>

@endsection