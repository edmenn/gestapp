@extends('app')

@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 1.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <div class="row card shadow p-3 m-3 mt-5">
                <div class="titulo col-lg-12">
                    <div class="float-start">
                        <h2>Añadir un nuevo Puerto</h2>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-primary" href="{{ route('proveedores.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                    </div>
                </div>
                <div class="col-lg-12 margin-tb">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hubieron algunos problemas.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('puertos.store') }}" method="POST" >
                        @csrf
                
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group">
                                    <strong>Direccion:</strong>
                                    <input type="text" name="direccion" class="form-control" placeholder="Direccion del Puerto">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
