@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')
    <div class="card mb-3 mt-4 shadow">
        <div class="card-header">
            <h1 class="card-title text-center">{{ $proyecto->nombre }}</h1>
        </div>
        <div class="card-body">
            <div class="row pl-3 pr-3">
                <div class="col-4 ml-3">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Reponsable: </strong>{{ $proyecto->user->name }}</p>
                    </div>
                </div>
                <div class="col-4 mr-3">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Departamento: </strong>{{ $proyecto->departamento->nombre }}</p>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Fecha de creación: </strong>{{ date_format($proyecto->created_at, 'd/m/Y') }}</p>
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Estado: </strong>{{ $proyecto->estado->nombre }}</p>
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Costo: </strong>{{ $proyecto->costo }}</p>
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Contratado: </strong>{{ $proyecto->contratado }}</p>
                    </div>
                </div>
                <div class="col-12">
                    {{-- <div class="card shadow"> --}}
                        <p class="text-center mt-2 mb-2">{{ $proyecto->descripcion }}</p>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h3>SubProyectos de {{$proyecto->nombre}}</h3>
            </div>
            <div class="float-end">
                <a class="btn btn-primary" href="{{ route('proyecto.sub_proyectos.create', [$proyecto]) }}">
                    Añadir SubProyecto
                </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="shadow p-3 mb-5 bg-white rounded card-container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Costo</th>
                <th scope="col">Contratado</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($proyecto->sub_proyectos as $sub_proyecto)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $sub_proyecto->nombre }}</td>
                    <td>{{ $sub_proyecto->descripcion }}</td>
                    <td>{{ $sub_proyecto->costo }}</td>
                    <td>{{ $sub_proyecto->contratado }}</td>
                    {{-- <td>{{ date_format($proyecto->created_at, 'jS M Y') }}</td> --}}
                    <td style="width: 10%">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="{{ route('proyecto.sub_proyectos.show', [$proyecto, $sub_proyecto]) }}" class="btn btn-outline-primary">Ver</a>
                            <a href="{{ route('proyecto.sub_proyectos.edit', [$proyecto, $sub_proyecto]) }}" class="btn btn-outline-primary">Editar</a>
                            <form action="{{ route('proyecto.sub_proyectos.destroy', [$proyecto, $sub_proyecto]) }}" method="POST" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                
                                <button type="submit" title="delete" class="btn btn-outline-danger">
                                    Eliminar
                                </button>
                            </form>
                            {{-- <button type="button" class="btn btn-danger">Eliminar</button> --}}
                          </div>
                        {{-- <a href="{{ route('proyectos.show', $proyecto->id) }}">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>
                        <a href="{{ route('proyectos.edit', $proyecto->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>
                        </a> --}}
                        {{-- <form action="{{ route('proyectos.destroy', $proyecto->id) }}" method="POST" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            
                            <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                <i class="fas fa-trash fa-lg text-danger"></i>
                            </button>
                        </form> --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>

@endsection