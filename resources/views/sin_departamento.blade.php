@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')
    <div class="titulo row shadow">
        <div class="mt-7 col-12">
            <div class="d-flex text-center justify-content-center my-3">
                <h2>Oops! No te asignaron a un departamento aún.</h2>
            </div>
            <br>
            <div class="d-flex text-center justify-content-center mt-3 mb-5 pd-3">
                <h2>Contacta con el administrador para que te asignen a uno y asi poder usar la plataforma</h2>
            </div>
        </div>
    </div>

@endsection