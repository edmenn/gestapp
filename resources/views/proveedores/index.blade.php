@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h2>Proveedores</h2>
            </div>
            @if (Auth::user()->hasRole('Admin'))
                <div class="float-end">
                    <a class="btn btn-primary" href="{{ route('proveedores.create') }}">
                        Agregar Proveedor
                    </a>
                </div>
            @endif
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="shadow p-3 mb-5 bg-white rounded card-container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Razon Social</th>
                <th scope="col">Nombre de Fantasia</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($proveedores as $proveedor)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $proveedor->razon_social }}</td>
                    <td>{{ $proveedor->nombre_fantasia }}</td>
                    <td style="width: 10%">
                        @if (Auth::user()->hasRole('Admin'))
                            <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                <a href="{{ route('proveedores.edit', ['proveedore' => $proveedor]) }}" class="btn btn-outline-primary">Ver</a>
                                <a href="{{ route('proveedores.edit', ['proveedore' => $proveedor]) }}" class="btn btn-outline-primary">Editar</a>
                                <form action="{{ route('proveedores.destroy', ['proveedore' => $proveedor]) }}" method="POST" style="display: inline-block">
                                    @csrf
                                    @method('DELETE')
                                    
                                    <button type="submit" title="delete" class="btn btn-outline-danger">
                                        Eliminar
                                    </button>
                                </form>
                            </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>

@endsection