@extends('app')

@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 1.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <div class="row card shadow p-3 m-3 mt-5">
                <div class="titulo col-lg-12">
                    <div class="float-start">
                        <h2>Añadir un nuevo Proyecto</h2>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-primary" href="{{ route('proyectos.index') }}" title="Atrás"> <i class="fas fa-backward "></i> </a>
                    </div>
                </div>
                <div class="col-lg-12 margin-tb">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hubieron algunos problemas.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('departamento.proyectos.store', [$departamento]) }}" method="POST" >
                        @csrf
                
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input type="text" name="nombre" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Descripción:</strong>
                                    <textarea class="form-control" style="height:50px" name="descripcion"
                                        placeholder="Introduction"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Costo:</strong>
                                    <input class="form-control" type="number" id="costo" name="costo">
                                    <small>Obs: Ingresar monto en guaraníes.</small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group mb-3">
                                    <strong>Seleccionar un responsable:</strong>
                                    <select required name="user_id" class="form-control">
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group mb-3">
                                    <strong>Seleccionar un puerto:</strong>
                                    <select required name="puerto_id" class="form-control">
                                        @foreach ($puertos as $puerto)
                                            <option value="{{ $puerto->id }}">{{ $puerto->nombre }}</option>
                                        @endforeach
                                    </select>
                                    @error('puerto')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
