@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h2>SubProyectos de </h2>
            </div>
            <div class="float-end">
                <a class="btn btn-primary" href="{{ route('proyectos.create') }}">
                    Añadir SubProyecto
                </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="shadow p-3 mb-5 bg-white rounded card-container table-responsive">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Costo</th>
                <th scope="col">Contratado</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($proyectos as $proyecto)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $proyecto->nombre }}</td>
                    <td>{{ $proyecto->direccion }}</td>
                    <td>{{ $proyecto->user->name }}</td>
                    <td>{{ $proyecto->estado }}</td>
                    <td>{{ number_format($proyecto->costo, 0, ',', '.') }}</td>
                    <td>{{ number_format($proyecto->contratado, 2, ',', '.') }}</td>
                    {{-- <td>{{ date_format($proyecto->created_at, 'jS M Y') }}</td> --}}
                    <td style="width: 10%">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="{{ route('proyectos.show', $proyecto->id) }}" class="btn btn-info">Ver</a>
                            <button href="{{ route('proyectos.edit', $proyecto->id) }}" class="btn btn-primary">Editar</button>
                            <form action="{{ route('proyectos.destroy', $proyecto->id) }}" method="POST" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                
                                <button type="submit" title="delete" class="btn btn-danger">
                                    Eliminar
                                </button>
                            </form>
                            {{-- <button type="button" class="btn btn-danger">Eliminar</button> --}}
                          </div>
                        {{-- <a href="{{ route('proyectos.show', $proyecto->id) }}">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>
                        <a href="{{ route('proyectos.edit', $proyecto->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>
                        </a> --}}
                        {{-- <form action="{{ route('proyectos.destroy', $proyecto->id) }}" method="POST" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            
                            <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                <i class="fas fa-trash fa-lg text-danger"></i>
                            </button>
                        </form> --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>

@endsection