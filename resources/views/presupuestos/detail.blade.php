@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
        a {
            color: #212529
        }
    </style>
@endsection
@section('content')
    <div class="card mb-3 mt-4 shadow">
        <div class="card-header">
            <h1 class="card-title text-center">{{ $subProyecto->nombre }}</h1>
        </div>
        <div class="card-body">
            <div class="row pl-3 pr-3">
                <div class="col-6 mr-3">
                    <a href=" {{ route('proyectos.show', [$proyecto]) }} ">
                        <div class="card shadow">
                            <p class="text-center mt-2 mb-2"><strong>Proyecto: </strong>{{ $proyecto->nombre }}</p>
                        </div>
                    </a>
                </div>
                <div class="col-6">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Fecha de creación: </strong>{{ date_format($subProyecto->created_at, 'd/m/Y') }}</p>
                    </div>
                </div>
                <div class="col-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Costo: </strong>{{ $subProyecto->costo }}</p>
                    </div>
                </div>
                <div class="col-6 mt-2">
                    <div class="card shadow">
                        <p class="text-center mt-2 mb-2"><strong>Contratado: </strong>{{ $subProyecto->contratado }}</p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card shadow mt-2">
                        <p class="text-center mt-2 mb-2">{{ $subProyecto->descripcion }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="d-flex justify-content-center">
                @if ($subProyecto->presupuesto_id)
                    <div class="justify-content-center">
                        <a class="btn btn-primary" href="{{ route('proyecto.sub_proyectos.create', [$proyecto]) }}">
                            Adjuntar Pago 
                        </a>
                    </div>
                @else
                    <div class="justify-content-center">
                        <a class="btn btn-primary" href="{{ route('proyecto.sub_proyectos.create', [$proyecto]) }}">
                            Agregar Presupuesto
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="titulo row">
        <div class="mt-7 col-12">
            <div class="float-start">
                <h3>Pagos de {{ $subProyecto->nombre }}</h3>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    {{-- <div class="shadow p-3 mb-5 bg-white rounded card-container">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Costo</th>
                <th scope="col">Contratado</th>
                <th style="width: 10%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($proyecto->sub_proyectos as $sub_proyecto)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $sub_proyecto->nombre }}</td>
                    <td>{{ $sub_proyecto->descripcion }}</td>
                    <td>{{ number_format($sub_proyecto->costo , 0, ',', '.') }}</td>
                    <td>{{ number_format($sub_proyecto->contratado 2, ',', '.') }}</td>
                    <td style="width: 10%">
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="{{ route('proyecto.sub_proyectos.show', [$proyecto, $sub_proyecto]) }}" class="btn btn-outline-primary">Ver</a>
                            <a href="{{ route('proyecto.sub_proyectos.edit', [$proyecto, $sub_proyecto]) }}" class="btn btn-outline-primary">Editar</a>
                            <form action="{{ route('proyecto.sub_proyectos.destroy', [$proyecto, $sub_proyecto]) }}" method="POST" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                
                                <button type="submit" title="delete" class="btn btn-outline-danger">
                                    Eliminar
                                </button>
                            </form>
                          </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div> --}}

@endsection