@extends('app')

@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 1.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <div class="row card shadow p-3 m-3 mt-5">
                <div class="titulo col-lg-12 margin-tb">
                    <div class="float-start">
                        <h2>Añadir un nuevo Presupuesto</h2>
                    </div>
                    <div class="float-end">
                        <a class="btn btn-primary" href="{{ route('proyecto.sub_proyectos.show', [$sub_proyecto->proyecto,$sub_proyecto]) }}" title="Go back"> <i class="fas fa-backward "></i> </a>
                    </div>
                </div>
            {{-- </div> --}}
            {{-- <div class="row"> --}}
                <div class="col-lg-12 margin-tb">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hubieron algunos problemas.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('sub_proyecto.presupuestos.store', [$sub_proyecto->id]) }}" method="POST" >
                        @csrf
                
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Concepto:</strong>
                                    <input type="text" name="concepto" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group mb-3">
                                    <strong>Selecciona un Proveedor:</strong>
                                    <select required name="proveedor_id" class="form-control">
                                        @foreach ($proveedores as $proveedor)
                                            <option style="color: black" value="{{ $proveedor->id }}">{{ $proveedor->nombre_fantasia }}</option>
                                        @endforeach
                                    </select>
                                    @error('proveedor_id')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Monto:</strong>
                                    <input class="form-control" type="number" id="costo" name="monto">
                                    <small>Monto total de sub-proyecto es: {{ $sub_proyecto->costo }} PYG</small>
                                    <br>
                                    <small>Dolar compra el dia de hoy es: {{ $dolarCompra }} PYG </small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
