<div class="offcanvas offcanvas-start w-25 bg-sidebar" tabindex="-1" id="offcanvas" data-bs-keyboard="false" data-bs-scroll="true" data-bs-backdrop="true">
    <div class="offcanvas-header row pb-0">
        {{-- <div class="row"> --}}
            <div class="col-10">
                <img src="{{ asset('logo/gestapp_logo1.png') }}" alt="" width="100" height="72" class="d-inline-block align-text-top float-start">
            </div>
            <div class="col-2">
                <button type="button" class="btn-close text-reset pt-0" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="col-12 pt-2">
                <h6 class="offcanvas-title d-none d-sm-block" id="offcanvas">Menu</h6>
            </div>
        {{-- </div> --}}
        {{-- <img src="{{ asset('logo/gestapp_logo1.png') }}" alt="" width="60" height="48" class="d-inline-block align-text-top">
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        <h6 class="offcanvas-title d-none d-sm-block" id="offcanvas">Menu</h6> --}}
    </div>
    <div class="offcanvas-body px-0">
        <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-start" id="menu">
            @if (Auth::user()->hasRole('Admin'))
                <li class="nav-item">
                    <a href="{{ route('proyectos.index') }}" class="nav-link text-truncate letra-sidebar">
                        <i class="fs-5 bi-house"></i><span class="ms-1 d-none d-sm-inline">Home</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('proyectos.index') }}" class="nav-link text-truncate letra-sidebar">
                        <i class="fs-5 bi-table"></i><span class="ms-1 d-none d-sm-inline">Proyectos</span></a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}" class="nav-link text-truncate letra-sidebar">
                        <i class="fs-5 bi-people"></i><span class="ms-1 d-none d-sm-inline">Usuarios</span> </a>
                </li>
                <li>
                    <a href="{{ route('roles.index') }}" class="nav-link text-truncate letra-sidebar">
                          <i class="fs-5 bi bi-person-check"></i><span class="ms-1 d-none d-sm-inline">Roles</span></a>
                </li>
                <li>
                    <a href="{{ route('departamentos.index') }}" class="nav-link text-truncate letra-sidebar">
                        <i class="fs-5 bi-grid"></i><span class="ms-1 d-none d-sm-inline">Departamentos</span></a>
                </li>
                <li>
                    <a href="{{ route('proveedores.index') }}" class="nav-link text-truncate letra-sidebar">
                        <i class="fs-5 bi bi-briefcase"></i><span class="ms-1 d-none d-sm-inline">Proveedores</span></a>
                </li>
                <li>
                    <a href="{{ route('puertos.index') }}" class="nav-link text-truncate letra-sidebar">
                        <img src="{{ asset('logo/anchor-icon.png') }}" alt="" width="17" height="17"><span class="ms-1 d-none d-sm-inline">Puertos</span></a>
                </li>
                <li>
                    <a href="{{ route('auditorias') }}" class="nav-link text-truncate letra-sidebar">
                        <i class="fs-5 bi-table"></i><span class="ms-1 d-none d-sm-inline">Auditoria</span></a>
                </li>
            @elseif (Auth::user()->hasRole('Funcionario'))
                @if(Auth::user()->departamento)
                    <li class="nav-item">
                        <a href="{{ route('departamento.proyectos.index', [Auth::user()->departamento]) }}" class="nav-link text-truncate letra-sidebar">
                            <i class="fs-5 bi-house"></i><span class="ms-1 d-none d-sm-inline">Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('departamentos.show', Auth::user()->departamento->id) }}" class="nav-link text-truncate letra-sidebar">
                            <i class="fs-5 bi-grid"></i><span class="ms-1 d-none d-sm-inline"> Departamento de {{ Auth::user()->departamento->nombre }}</span></a>
                    </li>
                @endif
            @endif
            {{-- <li class="dropdown">
                <a href="#" class="nav-link dropdown-toggle  text-truncate letra-sidebar" id="dropdown" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fs-5 bi-bootstrap"></i><span class="ms-1 d-none d-sm-inline">Bootstrap</span>
                </a>
                <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdown">
                    <li><a class="dropdown-item" href="#">New project...</a></li>
                    <li><a class="dropdown-item" href="#">Settings</a></li>
                    <li><a class="dropdown-item" href="#">Profile</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item letra-sidebar" href="#">Sign out</a></li>
                </ul>
            </li> --}}
            {{-- <li>
                <a href="#" class="nav-link text-truncate letra-sidebar">
                    <i class="fs-5 bi-speedometer2"></i><span class="ms-1 d-none d-sm-inline">Dashboard</span> </a>
            </li> --}}
        </ul>
    </div>
</div>