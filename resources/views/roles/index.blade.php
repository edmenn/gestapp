@extends('app')
@section('styles')
    <style>
        .card-container{
            background-color: #fff;
            /* width: 100%; */

        }
        .titulo{
            margin-top: 3.5rem; 
            margin-bottom: 1.5rem; 
        }
    </style>
@endsection
@section('content')

<div class="titulo row">
    <div class="mt-7 col-12">
        <div class="float-start">
            <h3>Administración de Roles</h3>
        </div>
        <div class="float-end">
            <a class="btn btn-primary" href="{{ route('roles.create') }}">
                Agregar Rol
            </a>
        </div>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
  @endif

  <div class="shadow p-3 mb-5 bg-white rounded card-container">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th style="width: 10%">Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($roles as $key => $role)
            <tr>
                <th scope="row">{{ $i++ }}</th>
                <td>{{ $role->name }}</td>
                <td style="width: 10%">
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <a href="{{ route('roles.show', $role->id) }}" class="btn btn-outline-primary">Ver</a>
                        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-outline-primary">Editar</a>
                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger']) !!}
                        {!! Form::close() !!}
                      </div>
                </td>
            </tr>
          @endforeach
        </tbody>
      </table>
  </div>

  {!! $roles->render() !!}

@endsection