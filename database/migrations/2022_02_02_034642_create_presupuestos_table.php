<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->id();
            $table->string('concepto',200);
            $table->unsignedBigInteger('subproyecto_id')->unsigned();
            $table->foreign('subproyecto_id')->references('id')->on('sub_proyectos')->cascadeOnUpdate()->cascadeOnDelete();
            $table->unsignedBigInteger('proveedor_id')->unsigned()->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedors')->cascadeOnUpdate()->nullOnDelete();
            $table->unsignedBigInteger('moneda_id')->unsigned()->nullable();
            // $table->foreign('moneda_id')->references('id')->on('monedas')->cascadeOnUpdate()->nullOnDelete();
            $table->decimal('monto', $precision = 12, $scale = 2);
            $table->decimal('cotizacion_dia', $precision = 7, $scale = 2);
            $table->decimal('monto_usd', $precision = 12, $scale = 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}
