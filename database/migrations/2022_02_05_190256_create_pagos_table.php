<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->id();
            $table->string('concepto',200);
            $table->unsignedBigInteger('presupuesto_id')->unsigned()->nullable();
            $table->foreign('presupuesto_id')->references('id')->on('presupuestos')->cascadeOnUpdate()->cascadeOnDelete();
            $table->unsignedBigInteger('file_id')->unsigned()->nullable();
            $table->foreign('file_id')->references('id')->on('files')->cascadeOnUpdate()->nullOnDelete();
            $table->unsignedBigInteger('moneda_id')->unsigned()->nullable();
            $table->foreign('moneda_id')->references('id')->on('monedas')->cascadeOnUpdate()->nullOnDelete();
            $table->decimal('monto', $precision = 12, $scale = 2)->nullable();
            // $table->decimal('cotizacion_dia', $precision = 7, $scale = 2)->nullable();
            $table->decimal('monto_usd', $precision = 12, $scale = 2)->nullable();
            $table->decimal('monto_iva', $precision = 12, $scale = 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
