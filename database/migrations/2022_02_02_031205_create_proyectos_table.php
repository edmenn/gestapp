<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',100)->nullable();
            $table->string('descripcion',255)->nullable();
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnUpdate()->nullOnDelete();
            $table->unsignedBigInteger('puerto_id')->unsigned()->nullable();
            $table->foreign('puerto_id')->references('id')->on('puertos');
            $table->unsignedBigInteger('departamento_id')->unsigned()->nullable();
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->unsignedBigInteger('estado_id')->unsigned()->nullable();
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->decimal('costo', $precision = 12, $scale = 2)->nullable();
            $table->decimal('contratado', $precision = 12, $scale = 2)->nullable();
            $table->decimal('cotizacion_dia', $precision = 7, $scale = 2)->nullable();
            $table->decimal('monto_usd', $precision = 12, $scale = 2)->nullable();
            $table->timestamps();
        });

        // Schema::table('proyectos', function (Blueprint $table) {
        //     // $table->id();
        //     // $table->string('nombre',100);
        //     // $table->string('direccion',255);
        //     $table->integer('responsable')->unsigned();
        //     $table->foreign('responsable')->references('id')->on('users');
        //     $table->integer('puerto_id')->unsigned();
        //     $table->foreign('puerto_id')->references('id')->on('puertos');
        //     $table->integer('departamento_id')->unsigned();
        //     $table->foreign('departamento_id')->references('id')->on('departamentos');
        //     $table->integer('estado_id')->unsigned();
        //     $table->foreign('estado_id')->references('id')->on('estados');
        //     // $table->decimal('costo', $precision = 12, $scale = 2);
        //     // $table->decimal('contratado', $precision = 12, $scale = 2);
        //     // $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
