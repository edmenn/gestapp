<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileToPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagos', function (Blueprint $table) {
            // eliminamos la columna file_id
            $table->dropForeign('pagos_file_id_foreign');
            $table->dropColumn('file_id');
            // agregamos la columna file
            $table->string('file', 200);
        });
        // eliminamos la tabla files
        Schema::drop('files');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Volvemos a crear la tabla files
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('file_path')->nullable();
            $table->string('type')->nullable();
            $table->string('size')->nullable();
            $table->timestamps();
            
        });
        // Modificamos la tabla pagos
        Schema::table('pagos', function (Blueprint $table) {
            // eliminamos la columna file
            $table->dropColumn('file');
            // agregamos la columna file_id y su referencia
            $table->unsignedBigInteger('file_id')->unsigned()->nullable();
            $table->foreign('file_id')->references('id')->on('files')->cascadeOnUpdate()->nullOnDelete();
        });
    }
}
