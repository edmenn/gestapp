<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_proyectos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',100)->nullable();
            $table->string('descripcion',255)->nullable();
            $table->unsignedBigInteger('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos')->cascadeOnUpdate()->cascadeOnDelete();
            $table->decimal('costo', $precision = 12, $scale = 2)->nullable();
            $table->decimal('contratado', $precision = 12, $scale = 2)->nullable();
            $table->decimal('cotizacion_dia', $precision = 7, $scale = 2)->nullable();
            $table->decimal('monto_usd', $precision = 12, $scale = 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_proyectos');
    }
}
