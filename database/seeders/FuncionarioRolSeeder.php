<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FuncionarioRolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'user-list',
            'user-edit',
            'proyecto-list',
            'sub_proyecto-list',
            'sub_proyecto-create',
            'sub_proyecto-edit',
            'sub_proyecto-delete',
            'puerto-list',
            'proveedor-list',
            'proveedor-create',
            'moneda-list',
            'estado-list',
            'departamento-list',
            'presupuesto-list',
            'presupuesto-create',
            'presupuesto-edit',
            'presupuesto-delete',
            'pago-list',
            'pago-create',
         ];

         $role = Role::create(['name' => 'Funcionario']);

         foreach ($permissions as $permission_name) {
            $permission = Permission::all()->firstWhere('name', $permission_name);
            $permission->assignRole($role);
         }

    }
}
