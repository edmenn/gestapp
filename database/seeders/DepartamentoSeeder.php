<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Departamento;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamentos = [
            'Informática',
            'Logistica',
            'Mantenimiento',
            'Recursos Humanos'
         ];

         foreach ($departamentos as $departamento) {
            Departamento::create(['nombre' => $departamento]);
         }
    }
}
