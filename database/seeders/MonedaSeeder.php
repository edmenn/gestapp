<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Moneda;

class MonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $monedas = [
            'USD',
            'PYG',
         ];

         foreach ($monedas as $moneda) {
            Moneda::create(['nombre' => $moneda]);
       }
    }
}
