<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'proyecto-list',
            'proyecto-create',
            'proyecto-edit',
            'proyecto-delete',
            'sub_proyecto-list',
            'sub_proyecto-create',
            'sub_proyecto-edit',
            'sub_proyecto-delete',
            'puerto-list',
            'puerto-create',
            'puerto-edit',
            'puerto-delete',
            'proveedor-list',
            'proveedor-create',
            'proveedor-edit',
            'proveedor-delete',
            'moneda-list',
            'moneda-create',
            'moneda-edit',
            'moneda-delete',
            'estado-list',
            'estado-create',
            'estado-edit',
            'estado-delete',
            'departamento-list',
            'departamento-create',
            'departamento-edit',
            'departamento-delete',
            'presupuesto-list',
            'presupuesto-create',
            'presupuesto-edit',
            'presupuesto-delete',
            'pago-list',
            'pago-create',
            'pago-edit',
            'pago-delete',
         ];

         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}