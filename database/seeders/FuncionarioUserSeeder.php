<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Departamento;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FuncionarioUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamento_mantenimiento = Departamento::all()->firstWhere('nombre', 'Mantenimiento');
        $departamento_logistica = Departamento::all()->firstWhere('nombre', 'Logistica');
        $departamento_informatica = Departamento::all()->firstWhere('nombre', 'Informática');

        $role = Role::all()->firstWhere('name', 'Funcionario');

        $user1 = User::firstOrCreate([
            'email' => 'edgar@gmail.com',
            'name' => 'Edgar',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_mantenimiento->id
        ]);
        $user1->assignRole([$role->id]);

        $user2 = User::firstOrCreate([
            'email' => 'luis@gmail.com',
            'name' => 'Luis Benitetz',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_mantenimiento->id
        ]);
        $user2->assignRole([$role->id]);

        $user3 = User::firstOrCreate([
            'email' => 'juan@gmail.com',
            'name' => 'Juan Valdez',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_mantenimiento->id
        ]);
        $user3->assignRole([$role->id]);

        $user4 = User::firstOrCreate([
            'email' => 'manuel@gmail.com',
            'name' => 'Manuel Ruiz',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_logistica->id
        ]);
        $user4->assignRole([$role->id]);

        $user5 = User::firstOrCreate([
            'email' => 'clara@gmail.com',
            'name' => 'Clara Alarcon',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_logistica->id
        ]);
        $user5->assignRole([$role->id]);

        $user6 = User::firstOrCreate([
            'email' => 'mauri@gmail.com',
            'name' => 'Mauricio Delgado',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_logistica->id
        ]);
        $user6->assignRole([$role->id]);

        $user7 = User::firstOrCreate([
            'email' => 'felix@gmail.com',
            'name' => 'Felix Zarate',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_informatica->id
        ]);
        $user7->assignRole([$role->id]);

        $user8 = User::firstOrCreate([
            'email' => 'aurelio@gmail.com',
            'name' => 'Aurelio Figueredo',
            'password' => Hash::make('gestapp123'),
            'departamento_id' => $departamento_informatica->id
        ]);
        $user8->assignRole([$role->id]);

    }
}
