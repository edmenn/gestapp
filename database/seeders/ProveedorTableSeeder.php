<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Proveedor;

class ProveedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proveedores = [
            ['PROYCON','PROYCON S.A.'],
            ['HARD TECH','HARD TECH S.A.'],
            ['INFOMARKET','INFOMARKET'],
            ['Logicalis','Logicalis S.A'],
         ];

         foreach ($proveedores as $proveedor) {
            Proveedor::create(['nombre_fantasia' => $proveedor[0], 'razon_social' => $proveedor[1]]);
         }
    }
}
