<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = [
            'Pendiente',
            'En Proceso',
            'Finalizado',
         ];

         foreach ($estados as $estado) {
            Estado::create(['nombre' => $estado]);
       }
    }
}
