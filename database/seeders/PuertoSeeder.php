<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Puerto;

class PuertoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $puertos = [
            ['San Antonio','Situado en San Antonio, Río Paraguay.'],
            ['Concepción','Situado en Concepción, Río Paraguay.'],
            ['Paredón','Situado en Hohenau, Río Parana.'],
            ['Rosario','Situado en San Pedro, Río Paraguay.']
         ];

         foreach ($puertos as $puerto) {
              Puerto::create(['nombre' => $puerto[0], 'direccion' => $puerto[1]]);
         }
    }
}
