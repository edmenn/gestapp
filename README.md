<p align="center"><img src="./public/logo/gestapp_logo2.png" width="250" heigth="250"></p>

# Pasos para instalar el proyecto

Resumen general:
- Actualizar composer
- Instalar todos las depencias del proyecto
- Correr php artisan migrate
- Por ultimo correr uno por los seeder en la secuencia que se presentarán después.

## 1. Actualizar composer:
```
composer upgrade
```

## 2. Instalar todos las depencias del proyecto
Primero instalaremos Laravel Breeze (dependencia para login/registro) con los siguientes comandos:
```
composer require laravel/breeze --dev
php artisan breeze:install
npm install
npm run dev
```

Luego instalaremos Laravel Spatie (para aministracion de permisos y roles) con los siguientes comandos:
```
composer require spatie/laravel-permission
composer require laravelcollective/html
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
```

## 3. Hacer las migraciones:
```
php artisan migrate
```

## 4. Poblar la base de datos con los semilleros:
**Ojo!** Correr los semilleros en el mismo orden que se presentan a continuacion: 
```
php artisan db:seed --class=DepartamentoSeeder
php artisan db:seed --class=EstadoSeeder
php artisan db:seed --class=PuertoSeeder
php artisan db:seed --class=MonedaSeeder
php artisan db:seed --class=ProveedorTableSeeder
php artisan db:seed --class=PermissionTableSeeder
php artisan db:seed --class=AdminTableSeeder
php artisan db:seed --class=FuncionarioRolSeeder
php artisan db:seed --class=FuncionarioUserSeeder
```