<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Proyecto;
use App\Models\Presupuesto;

class SubProyecto extends Model
{
    use HasFactory;

    protected $fillable = [
        'proyecto_id',
        'nombre',
        'descripcion',
        'costo',
        'cotizacion_dia',
        'monto_usd',
        'contratado',
    ];

    public function proyecto()
    {
        return $this->belongsTo(Proyecto::class);
    }
    public function presupuesto()
    {
        return $this->hasOne(Presupuesto::class, 'subproyecto_id');
    }
}
