<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Proyecto;

class Departamento extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
    ];
    public function usuarios()
    {
        return $this->hasMany(User::class);
    }
    public function proyectos()
    {
        return $this->hasMany(Proyecto::class);
    }
}
