<?php

namespace App\Models;
use App\Models\Presupuesto;
use App\Models\Moneda;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

    protected $fillable = [
        'concepto',
        'presupuesto_id',
        'moneda_id',
        'monto',
        'cotizacion_dia',
        'monto_usd',
        'monto_iva',
        'file',
    ];

    public function presupuesto()
    {
        return $this->belongsTo(Presupuesto::class);
    }

    public function moneda()
    {
        return $this->belongsTo(Moneda::class);
    }
}
