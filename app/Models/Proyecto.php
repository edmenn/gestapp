<?php

namespace App\Models;
use App\Models\Puerto;
use App\Models\User;
use App\Models\Departamento;
use App\Models\Estado;
use App\Models\SubProyecto;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'direccion',
        'user_id',
        'puerto_id',
        'departamento_id',
        'estado_id',
        'costo',
        'cotizacion_dia',
        'monto_usd',
        'contratado',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function puerto()
    {
        return $this->belongsTo(Puerto::class);
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }

    public function sub_proyectos()
    {
        return $this->hasMany(SubProyecto::class);
    }
    public function costo_lleno(){
        $bandera = false;
        $costo_total = 0;
        foreach ($this->sub_proyectos as $sub_p) {
            $costo_total += $sub_p->costo;
        }
        if($costo_total == $this->costo){
            $bandera = true;
        }
        return $bandera;
    }
}
