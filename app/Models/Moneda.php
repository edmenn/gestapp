<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;
use App\Models\Pago;

class Moneda extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
    ];
    public function presupuestos()
    {
        return $this->hasMany(Presupuesto::class);
    }
    public function pagos()
    {
        return $this->hasMany(Pago::class);
    }
}
