<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Proyecto;

class Estado extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
    ];
    public function proyectos()
    {
        return $this->hasMany(Proyecto::class);
    }
}
