<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Presupuesto;

class Proveedor extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_fantasia',
        'razon_social',
    ];
    public function presupuestos()
    {
        return $this->hasMany(Presupuesto::class);
    }
}
