<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\SubProyecto;
use App\Models\Proveedor;
use App\Models\Moneda;
use App\Models\Pago;

class Presupuesto extends Model
{
    use HasFactory;

    protected $fillable = [
        'concepto',
        'subproyecto_id',
        'proveedor_id',
        'moneda_id',
        'monto',
        'cotizacion_dia',
        'monto_usd',
    ];
    public function subproyecto()
    {
        return $this->belongsTo(SubProyecto::class, 'subproyecto_id');
    }
    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }
    public function moneda()
    {
        return $this->belongsTo(Moneda::class);
    }
    public function pagos()
    {
        return $this->hasMany(Pago::class);
    }
}
