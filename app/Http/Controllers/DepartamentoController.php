<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departamento;
use App\Models\User;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departamentos = Departamento::orderBy('id','DESC')->paginate(5);
        return view('departamentos.index',compact('departamentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::all()->where('departamento_id', '==', null);
        return view('departamentos.create',compact('usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|unique:departamentos,nombre',
            'usuarios' => 'required',
        ]);

        $departamento = Role::create(['nombre' => $request->input('nombre')]);
        $id_usaurios = $request->input('usuarios');
        foreach ($id_usaurios as $id) {
           $user = User::find($id);
           $user->departamento()->associate($departamento);
           $user->save();
        }
        // $role->syncPermissions($request->input('usuarios'));

        return redirect()->route('departamentos.index')
                        ->with('success','Departamento creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function show(Departamento $departamento)
    {
        $chart_options = [
            'chart_title' => 'Estados por proyecto',
            'chart_type' => 'pie',
            'report_type' => 'group_by_relationship',
            'model' => 'App\Models\Proyecto',
        
            'relationship_name' => 'estado', // represents function estado() on Transaction model
            'group_by_field' => 'nombre', // estados.nombre
        
            'aggregate_function' => 'sum',
            'aggregate_field' => 'amount',
            
        ];

        $chart = new LaravelChart($chart_options);
        return view('departamentos.show', compact('departamento','chart'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function edit(Departamento $departamento)
    {
        $usuarios = User::all()->where('departamento_id', '==', null)->concat(User::all()->where('departamento_id', '==', $departamento->id));
        // error_log("Usuarios es " . $usuarios);
        $usuariosDepartamento = $departamento->usuarios->pluck('id','id')->all();
        // error_log("usuariosDepartamento es " . print_r($usuariosDepartamento));
        return view('departamentos.edit',compact('departamento','usuarios','usuariosDepartamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departamento $departamento)
    {
        $this->validate($request, [
            'nombre' => 'unique:departamentos,nombre',
        ]);

        $id_usuarios = $request->input('usuarios');

        $usuarios = User::all();
        foreach ($usuarios as $user) {
            if( in_array($user->id, $id_usuarios)){
                $user->departamento()->associate($departamento);
            }
            else if($user->departamento){
                if($user->departamento->id == $departamento->id){
                    $user->departamento()->dissociate();
                }
            }
            $user->save();
        }
        // $role->syncPermissions($request->input('usuarios'));

        return redirect()->route('departamentos.index')
                        ->with('success','Departamento actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Departamento $departamento)
    {
        $departamento->delete();
        return redirect()->route('departamentos.index')
        ->with('success','El departamento ha sido eliminado exitosamente.');
    }
}
