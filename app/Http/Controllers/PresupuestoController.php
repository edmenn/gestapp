<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Presupuesto;
use App\Models\SubProyecto;
use App\Models\Proyecto;
use App\Models\User;
use App\Models\Puerto;
use App\Models\Moneda;
use App\Models\Departamento;
use App\Models\Proveedor;
use App\Models\Auditoria;
use Illuminate\Http\Request;

class PresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SubProyecto $sub_proyecto)
    {
        $proveedores = Proveedor::all();
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        return view('presupuestos.create', compact('sub_proyecto','proveedores','dolarCompra'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SubProyecto $sub_proyecto)
    {
        $proveedores = Proveedor::all();
        $dolar = $this->dolar();

        $validator = Validator::make($request->all(), [
            'concepto' => 'required|max:200',
            'proveedor_id' => 'required|exists:proveedors,id',
            'monto' => 'required|numeric',
        ]);

        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];

        if($request->input('monto') > $sub_proyecto->costo){
            $validator->errors()->add('monto', 'El monto supera al monto permitido del sub-proyecto');
            return view('presupuestos.create', compact('sub_proyecto','proveedores','monedas','dolarCompra'))
            ->withErrors($validator);
        }

        $monto_usd = $request->input('monto')/$dolarCompra;
        
        // error_log("Moneda_id es: " . $request->input('moneda_id'));
        // if ($moneda_dolar->id != $request->input('moneda_id')) {
        //     $monto_usd = $request->input('monto')/$dolarCompra;
        // }
        // else{
        //     $monto_usd = $request->input('monto');
        // }

        $presupuesto = Presupuesto::create(array_merge($request->all(), ['subproyecto_id' => $sub_proyecto->id, 'cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]));

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'presupuestos',
        'model_id'=>$presupuesto->id,
        'action'=>'Añadió el presupuesto con concepto: ' . $presupuesto->concepto . ' al sub-proyecto: ' . $presupuesto->subproyecto->nombre]);

        return redirect()->route('proyecto.sub_proyectos.show',[$sub_proyecto->proyecto, $sub_proyecto])
        ->with('success','Presupuesto se ha añadido exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function show(SubProyecto $sub_proyecto, Presupuesto $presupuesto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function edit(SubProyecto $sub_proyecto, Presupuesto $presupuesto)
    {
        $proveedores = Proveedor::all();
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];

        return view('presupuestos.edit', compact('proveedores','sub_proyecto','presupuesto','dolarCompra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubProyecto $sub_proyecto, Presupuesto $presupuesto)
    {
        $proveedores = Proveedor::all();

        error_log("El antiguo valor de monto es: " . $presupuesto->monto);

        $input = $request->validate([
            'concepto' => 'max:200',
            'proveedor_id' => 'exists:proveedors,id',
            'monto' => 'numeric',
        ]);

        $validator = Validator::make($request->all(), [
            'concepto' => 'max:200',
            'proveedor_id' => 'exists:proveedors,id',
            'monto' => 'numeric',
        ]);

        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];

        if($request->input('monto') > $sub_proyecto->costo){
            $validator->errors()->add('monto', 'El monto supera al monto permitido del sub-proyecto');
            return view('presupuestos.create', compact('sub_proyecto','proveedores','dolarCompra'))
            ->withErrors($validator);
        }
        $suma_pagos = 0;
        foreach ($presupuesto->pagos as $pago) {
            $suma_pagos += $pago->monto;
        }
        if($request->input('monto') < $suma_pagos){
            $validator->errors()->add('monto', 'El monto ingresado no puede ser menor al monto que ya se pagó: ' . $suma_pagos . "PYG");
            return back()->withErrors($validator)->withInput();
        }


        $monto_usd = $request->input('monto')/$dolarCompra;
        
        // error_log("Moneda_id es: " . $request->input('moneda_id'));
        // if ($moneda_dolar->id != $request->input('moneda_id')) {
        //     $monto_usd = $request->input('monto')/$dolarCompra;
        // }
        // else{
        //     $monto_usd = $request->input('monto');
        // }
        $presupuesto->fill(array_merge($input, ['cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]))->save();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'presupuestos',
        'model_id'=>$presupuesto->id,
        'action'=>'Editó el presupuesto con concepto: ' . $presupuesto->concepto . ' del sub-proyecto: ' . $presupuesto->subproyecto->nombre]);

        return redirect()->route('proyecto.sub_proyectos.show',[$sub_proyecto->proyecto, $sub_proyecto])
        ->with('success','Presupuesto se ha actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubProyecto $sub_proyecto, Presupuesto $presupuesto)
    {
        $id_presupuesto = $presupuesto->id;
        $concepto = $presupuesto->concepto;
        $presupuesto->delete();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'presupuestos',
        'model_id'=>$id_presupuesto,
        'action'=>'Editó el presupuesto con concepto: ' . $concepto . ' del sub-proyecto: ' . $sub_proyecto->nombre]);

        return redirect()->route('proyecto.sub_proyectos.show',[$sub_proyecto->proyecto, $sub_proyecto])
        ->with('success','Presupuesto se ha eliminado exitosamente.');
    }

    public function dolar(){
        $url ="https://dolar.melizeche.com/api/1.0/"; 
 
        $ch = \curl_init($url);

        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        \curl_setopt_array( $ch, $options );
        
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        
        // Cerrar el recurso cURL y liberar recursos del sistema 
        \curl_close($ch);
        //print_r($response);
        
        $data = json_decode($response, true);     
        //var_dump($data);
        $fecha=$data['updated'];
        $date = strtotime($fecha);

        $MydcambiosCompra=$data['dolarpy']['mydcambios']['compra'];
        $MydcambiosVenta=$data['dolarpy']['mydcambios']['venta'];

        $resultado = array('dolarCompra'=>$MydcambiosCompra, 'dolarVenta'=> $MydcambiosVenta);
        return $resultado;
    }
}
