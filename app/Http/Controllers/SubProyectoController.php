<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\SubProyecto;
use App\Models\Proyecto;
use App\Models\User;
use App\Models\Puerto;
use App\Models\Departamento;
use App\Models\Estado;
use App\Models\Auditoria;

class SubProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Proyecto $proyecto)
    {
        $sub_proyectos = SubProyecto::latest()->paginate(5);

        return view('sub_proyectos.index', compact('sub_proyectos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Proyecto $proyecto)
    {   
        $costo_total = 0;
        foreach($proyecto->sub_proyectos as $sub){ 
            $costo_total += $sub->costo;
            error_log('\n' . $sub->costo);
        }
        $saldo_disponible = $proyecto->costo - $costo_total;
        return view('sub_proyectos.create', compact('proyecto','saldo_disponible'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Proyecto $proyecto, Request $request)
    {
        $request->validate([
            'nombre' => 'bail|required|max:200',
            'descripcion' => 'required|max:255',
            'costo' => 'required|numeric',
            // 'contratado' => 'required|numeric',
        ]);
        $costo_total = 0;
        $valido = 1;
        foreach($proyecto->sub_proyectos as $sub){ 
            $costo_total += $sub->costo;
            error_log('\n' . $sub->costo);
        }
        $saldo_disponible = $proyecto->costo - $costo_total;
        if($request->input('costo') > $saldo_disponible || $request->input('costo') > $proyecto->costo ){
            $valido = 0;
            error_log('Entra aca');
        }
        error_log('\nCosto total:' . $costo_total . '\n');
        error_log('\nCosto ingresado:' . $request->input('costo') . '\n');
        error_log('\nCosto del proyecto:' . $proyecto->costo . '\n');
        error_log('\nValido es:' . $valido . '\n');
        $validator = Validator::make($request->all(),
            [
                'costo' => [
                    'required',
                    'numeric',
                ],
            ]
        );

        if($valido == 0){
            $validator->errors()->add('costo', 'El costo supera el presupuesto permitido');
            return view('sub_proyectos.create', compact('proyecto','saldo_disponible'))
            ->withErrors($validator);
        }

        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $monto_usd = $request->input('costo')/$dolarCompra;

        $sub_proyecto = SubProyecto::create(array_merge($request->all(), ["proyecto_id"=>$proyecto->id, 'contratado'=>0, 'cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]));
        // $proyecto->contratado += $sub_proyecto->costo;

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'sub_proyectos',
        'model_id'=>$sub_proyecto->id,
        'action'=>'Añadió el sub-proyecto: ' . $sub_proyecto->nombre . ' al proyecto: ' . $sub_proyecto->proyecto->nombre]);

        $finalizado = Estado::firstWhere('nombre','Finalizado');
        $enProceso = Estado::firstWhere('nombre','En Proceso');
        if($sub_proyecto->proyecto->estado_id == $finalizado->id){
            $sub_proyecto->proyecto->estado_id = $enProceso->id;
            $sub_proyecto->proyecto->save();
        }

        return redirect()->route('proyectos.show', $proyecto)
        ->with('success','SubProyecto se ha creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto, SubProyecto $subProyecto)
    {
        return view('sub_proyectos.detail', compact('proyecto','subProyecto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyecto $proyecto, SubProyecto $subProyecto)
    {
        $costo_total = 0;
        foreach($proyecto->sub_proyectos as $sub){ 
            $costo_total += $sub->costo;
            error_log('\n' . $sub->costo);
        }
        $saldo_disponible = $proyecto->costo - $costo_total;
        return view('sub_proyectos.edit', compact('subProyecto', 'proyecto','saldo_disponible'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto, SubProyecto $sub_proyecto)
    {
        $input = $request->validate([
            'nombre' => 'max:200',
            'descripcion' => 'max:255',
            'costo' => 'numeric',
            // 'contratado' => 'required|numeric',
        ]);

        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $monto_usd = $request->input('costo')/$dolarCompra;

        $sub_proyecto->fill(array_merge($input, ['cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]))->save();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'sub_proyectos',
        'model_id'=>$sub_proyecto->id,
        'action'=>'Editó el sub-proyecto: ' . $sub_proyecto->nombre . ' del proyecto: ' . $sub_proyecto->proyecto->nombre]);

        return redirect()->route('proyectos.show',[$proyecto])
        ->with('success','SubProyecto se ha actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto, SubProyecto $subProyecto)
    {
        $nombre = $subProyecto->nombre;
        $subProyecto->delete();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'sub_proyectos',
        'model_id'=>$sub_proyecto->id,
        'action'=>'Eliminó el sub-proyecto: ' . $nombre . ' del proyecto: ' . $proyecto->nombre]);

        return redirect()->route('proyectos.show', $proyecto)
        ->with('success','SubProyecto se ha eliminado exitosamente.');
    }

    public function dolar(){
        $url ="https://dolar.melizeche.com/api/1.0/"; 
 
        $ch = \curl_init($url);

        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        \curl_setopt_array( $ch, $options );
        
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        
        // Cerrar el recurso cURL y liberar recursos del sistema 
        \curl_close($ch);
        //print_r($response);
        
        $data = json_decode($response, true);     
        //var_dump($data);
        $fecha=$data['updated'];
        $date = strtotime($fecha);

        $MydcambiosCompra=$data['dolarpy']['mydcambios']['compra'];
        $MydcambiosVenta=$data['dolarpy']['mydcambios']['venta'];

        $resultado = array('dolarCompra'=>$MydcambiosCompra, 'dolarVenta'=> $MydcambiosVenta);
        return $resultado;
    }
}
