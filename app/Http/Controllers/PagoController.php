<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Models\Pago;
use App\Models\Presupuesto;
use App\Models\User;
use App\Models\Puerto;
use App\Models\Departamento;
use App\Models\Estado;
use App\Models\Auditoria;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PagoController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:pago-list|pago-create|pago-edit|pago-delete', ['only' => ['index','store']]);
         $this->middleware('permission:pago-create', ['only' => ['create','store']]);
         $this->middleware('permission:pago-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:pago-delete', ['only' => ['destroy']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Presupuesto $presupuesto)
    {   
        // $monedas = Moneda::all();
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $dolarVenta = $dolar['dolarVenta'];
        return view('pagos.create', compact('presupuesto', 'dolarCompra'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Presupuesto $presupuesto, Request $request)
    {
        $rules = [
            'concepto' => 'required|max:200',
            'monto' => 'required|numeric',
        ];

        $dolar = $this->dolar();    //se llama la funcion dolar
        $dolarCompra = $dolar['dolarCompra'];

        $validator =  Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // verificamos que se haya cargado un archivo
        if(!$request->hasFile('file')){
            $validator = Validator::make($request->input(), []); // creamos un objeto validator
            $validator->errors()->add('file', 'El campo es requerido, debe ingresar un archivo WORD, PDF, EXCEL, JPEG, JPG o PNG.');
            return back()->withErrors($validator)->withInput();
        }

        // verificamos la extension del archivo cargado
        $extension = $request->file('file')->getClientOriginalExtension();
        if(!in_array($extension, array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'jpg', 'jpeg', 'png'))){
            $validator = Validator::make($request->input(), []); // Creamos un objeto validator
            $validator->errors()->add('file', 'El archivo introducido debe corresponder a alguno de los siguientes formatos: doc, docx, pdf, xls, xlsx, jpg ,jpeg, png.'); // Agregamos el error
            return back()->withErrors($validator)->withInput();
        }
        
        $costo_total = 0;   //costo total en guaranies
        $valido = 1;
        foreach($presupuesto->pagos as $pago){
            $costo_total += $pago->monto;
        }
        $monto_usd = $request->input('monto')/$dolarCompra;
        $monto_guaranies = $request->input('monto');
        $costo_total += $monto_guaranies;
        
        if($costo_total > $presupuesto->monto){
            $valido = 0;
            $validator->errors()->add('monto', 'Se introdujo un monto mayor al monto presupuestado');
            return back()->withErrors($validator)->withInput();
        }

        // Pasó todas las validaciones, guardamos el archivo
        $fileName = time().'-pago.'.$extension; // nombre a guardar
        // Cargamos el archivo (ruta storage/app/public/files, enlace simbolico desde public/storage/files)
        $path = $request->file('file')->storeAs('public/files', $fileName);

        $pago = new Pago;
        $pago->presupuesto_id = $presupuesto->id;
        $pago->concepto = $request->input('concepto');
        $pago->monto = $request->input('monto');
        $pago->cotizacion_dia = $dolarCompra;
        $pago->monto_usd = $monto_usd;
        $pago->monto_iva = intval(round($pago->monto * 0.1));
        $pago->file = $fileName;
        $pago->save();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'pagos',
        'model_id'=>$pago->id,
        'action'=>'Añadió el pago con concepto: ' . $presupuesto->concepto . ' al sub-proyecto: ' . $presupuesto->subproyecto->nombre]);

        $pendiente = Estado::firstWhere('nombre','Pendiente');
        $enProceso = Estado::firstWhere('nombre','En Proceso');
        $finalizado = Estado::firstWhere('nombre','Finalizado');
        if($presupuesto->subproyecto->proyecto->estado_id == $pendiente->id){
            $presupuesto->subproyecto->proyecto->estado_id = $enProceso->id;
            $presupuesto->subproyecto->proyecto->save();
        }
        error_log("Monto en guaranies es: " . $monto_guaranies);
        $presupuesto->subproyecto->contratado += $monto_guaranies;
        $presupuesto->subproyecto->save();

        $presupuesto->subproyecto->proyecto->contratado += $monto_guaranies;
        $presupuesto->subproyecto->proyecto->save();

        $proyecto = $presupuesto->subproyecto->proyecto;
        $terminado = 1;
        foreach($proyecto->sub_proyectos as $sub_proyecto){
            if($sub_proyecto->presupuesto){
                if($sub_proyecto->contratado < $sub_proyecto->presupuesto->monto){
                    $terminado = 0;
                }
            }
        }
        if($terminado == 1){
            $presupuesto->subproyecto->proyecto->estado_id = $finalizado->id;
            $presupuesto->subproyecto->proyecto->save();
        }

        return redirect()->route('proyecto.sub_proyectos.show', [$presupuesto->subproyecto->proyecto->id, $presupuesto->subproyecto->id])
        ->with('success','Pago se ha creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Presupuesto $presupuesto, Pago $pago)
    {
        return view('pagos.detail', compact('presupuesto', 'pago'));
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function edit(Presupuesto $presupuesto, Pago $pago)
    {
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];

        return view('pagos.edit', compact('pago','presupuesto','dolarCompra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Presupuesto  $presupuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Presupuesto $presupuesto, Pago $pago)
    {
        $rules = [
            'concepto' => 'required|max:200',
            'monto' => 'required|numeric',
        ];

        $input = $request->validate([
            'concepto' => 'required|max:200',
            'monto' => 'required|numeric',
        ]);

        $dolar = $this->dolar();    //se llama la funcion dolar
        $dolarCompra = $dolar['dolarCompra'];

        $validator =  Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $suma_pagos = 0;
        foreach ($presupuesto->pagos as $pago) {
            $suma_pagos += $pago->monto;
        }
        $suma_pagos -= $pago->monto;    //monto de la actual instancia de pago
        $suma_pagos += $request->input('monto');    //monto ingresado en el form de actualizacion

        if($suma_pagos > $presupuesto->monto){
            $validator->errors()->add('monto', 'El monto ingresado supera el monto presupuestado');
            return back()->withErrors($validator)->withInput();
        }

        $monto_usd = $request->input('monto')/$dolarCompra;
        $monto_guaranies = $request->input('monto');
        
        if(!empty($input['file'])){
            // verificamos la extension del archivo cargado
            $extension = $request->file('file')->getClientOriginalExtension();
            if(!in_array($extension, array('doc', 'docx', 'pdf', 'xls', 'xlsx', 'jpg', 'jpeg', 'png'))){
                $validator = Validator::make($request->input(), []); // Creamos un objeto validator
                $validator->errors()->add('file', 'El archivo introducido debe corresponder a alguno de los siguientes formatos: doc, docx, pdf, xls, xlsx, jpg ,jpeg, png.'); // Agregamos el error
                return back()->withErrors($validator)->withInput();
            }

            // Pasó todas las validaciones, guardamos el archivo
            $fileName = time().'-pago.'.$extension; // nombre a guardar
            // Cargamos el archivo (ruta storage/app/public/files, enlace simbolico desde public/storage/files)
            $path = $request->file('file')->storeAs('public/files', $fileName);

        }
        else{
            $input = Arr::except($input,array('file'));
        }

        //Si pasa todas las excepciones se actualiza el proyecto y el subproyecto
        $presupuesto->subproyecto->contratado -= $pago->monto;
        $presupuesto->subproyecto->contratado += $request->input('monto');
        $presupuesto->subproyecto->save();

        $presupuesto->subproyecto->proyecto->contratado -= $pago->monto;
        $presupuesto->subproyecto->proyecto->contratado += $request->input('monto');
        $presupuesto->subproyecto->proyecto->save();

        $monto_iva = intval(round($request->input('monto') * 0.1));
        
        $pago->fill(array_merge($input, ['cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd, 'monto_iva'=>$monto_iva]))->save();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'pagos',
        'model_id'=>$pago->id,
        'action'=>'Editó el pago con concepto: ' . $pago->concepto . ' del sub-proyecto: ' . $presupuesto->subproyecto->nombre]);

        return redirect()->route('proyecto.sub_proyectos.show',[$presupuesto->subproyecto->proyecto, $presupuesto->subproyecto])
        ->with('success','Presupuesto se ha actualizado exitosamente.');
    }

    /**
     * Funcionalidad para descargar archivo.
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Presupuesto $presupuesto, Pago $pago)
    {
        $nombre = Str::slug($pago->concepto); // se quitan los espacios y caracteres especiales
        $filename = explode(".", $pago->file);
        $extension = $filename[1];          // Obtenemos la extensión del archivo

        return Storage::download('public/files/'.$pago->file, $nombre .'.'. $extension);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presupuesto $presupuesto, Pago $pago)
    {
        // Eliminamos el archivo
        Storage::delete('public/files/'.$pago->file);

        // $dolar = $this->dolar();
        // $moneda_dolar = Moneda::all()->firstWhere('nombre', 'USD');
        // $dolarCompra = $dolar['dolarCompra'];
        // if($moneda_dolar->id != $pago->moneda_id) {
        //     $monto_usd = $pago->monto/$dolarCompra;
        //     $monto_guaranies = $pago->monto;
        // }
        // else{
        //     $monto_usd = $pago->monto;
        //     $monto_guaranies = $monto_usd * $dolarCompra;
        // }
        $presupuesto->subproyecto->contratado -= $pago->monto;
        $presupuesto->subproyecto->save();

        $id_pago = $pago->id;
        $pago_concepto = $pago->concepto;

        // Eliminamos el pago
        $pago->delete();

        Auditoria::create(['user_id'=>Auth::user()->id,
        'table'=>'pagos',
        'model_id'=>$id_pago,
        'action'=>'Elimino el pago con concepto: ' . $pago_concepto . ' al sub-proyecto: ' . $presupuesto->subproyecto->nombre]);

        return redirect()->route('proyecto.sub_proyectos.show', [$presupuesto->subproyecto->proyecto->id, $presupuesto->subproyecto->id])
        ->with('success','Pago se ha eliminado exitosamente.');
    }

    public function dolar(){
        $url ="https://dolar.melizeche.com/api/1.0/"; 
 
        $ch = \curl_init($url);

        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        \curl_setopt_array( $ch, $options );
        
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        
        // Cerrar el recurso cURL y liberar recursos del sistema 
        \curl_close($ch);
        //print_r($response);
        
        $data = json_decode($response, true);     
        //var_dump($data);
        $fecha=$data['updated'];
        $date = strtotime($fecha);

        $MydcambiosCompra=$data['dolarpy']['mydcambios']['compra'];
        $MydcambiosVenta=$data['dolarpy']['mydcambios']['venta'];

        $resultado = array('dolarCompra'=>$MydcambiosCompra, 'dolarVenta'=> $MydcambiosVenta);
        return $resultado;
    }
}
