<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use App\Models\Puerto;
use App\Models\Estado;
use App\Models\Departamento;
use Illuminate\Http\Request;

class DepProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Departamento $departamento)
    {
        // $proyectos = $departamento->proyectos->paginate(5);
        $proyectos = $departamento->proyectos;

        return view('dep_proyectos.index', compact('departamento','proyectos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Departamento $departamento)
    {
        $users = $departamento->usuarios;
        $puertos = Puerto::all();
        return view('dep_proyectos.create',compact('departamento','users','puertos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Departamento $departamento)
    {
        $request->validate([
            'nombre' => 'required|max:200',
            'descripcion' => 'required|max:255',
            'puerto_id' => 'required|exists:puertos,id',
            'user_id' => 'required|exists:users,id',
            'costo' => 'required|numeric',
            // 'contratado' => 'required|numeric',
        ]);
        $estado = Estado::firstOrCreate([
            'nombre' => 'Pendiente'
        ]);

        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $monto_usd = $request->input('costo')/$dolarCompra;


        Proyecto::create(array_merge($request->all(), ['departamento_id'=>$departamento->id , 'estado_id' => $estado->id, 'contratado'=>0, 'cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]));

        return redirect()->route('departamento.proyectos.index',$departamento)
        ->with('success','Proyecto se ha creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(Departamento $departamento, Proyecto $proyecto)
    {
        return view('dep_proyectos.detail', compact('proyecto'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(Departamento $departamento, Proyecto $proyecto)
    {
        $users = $departamento->usuarios;
        $estados = Estado::all();
        $puertos = Puerto::all();
        return view('dep_proyectos.edit', compact('proyecto','users','estados','puertos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departamento $departamento, Proyecto $proyecto)
    {
        $input = $request->validate([
            'nombre' => 'max:200',
            'descripcion' => 'max:200',
            'puerto_id' => 'exists:puertos,id',
            'user_id' => 'exists:users,id',
            'estado_id' => 'exists:estados,id',
            'costo' => 'numeric',
        ]);
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $monto_usd = $request->input('costo')/$dolarCompra;
        $proyecto->fill(array_merge($input, ['cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]))->save();
        return redirect()->route('departamento.proyectos.index', $departamento)
        ->with('success','Proyecto se ha actualizado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Departamento $departamento, Proyecto $proyecto)
    {
        $proyecto->delete();
        return redirect()->route('departamento.proyectos.index', $departamento)
        ->with('success','El proyecto ha sido eliminado exitosamente.');
    }

    public function dolar(){
        $url ="https://dolar.melizeche.com/api/1.0/"; 
 
        $ch = \curl_init($url);

        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        \curl_setopt_array( $ch, $options );
        
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        
        // Cerrar el recurso cURL y liberar recursos del sistema 
        \curl_close($ch);
        //print_r($response);
        
        $data = json_decode($response, true);     
        //var_dump($data);
        $fecha=$data['updated'];
        $date = strtotime($fecha);

        $MydcambiosCompra=$data['dolarpy']['mydcambios']['compra'];
        $MydcambiosVenta=$data['dolarpy']['mydcambios']['venta'];

        $resultado = array('dolarCompra'=>$MydcambiosCompra, 'dolarVenta'=> $MydcambiosVenta);
        return $resultado;
    }
}
