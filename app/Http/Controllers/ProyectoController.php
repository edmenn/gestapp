<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Models\Proyecto;
use App\Models\User;
use App\Models\Puerto;
use App\Models\Departamento;
use App\Models\Estado;
use App\Models\Auditoria;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;

class ProyectoController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:proyecto-list|proyecto-create|proyecto-edit|proyecto-delete', ['only' => ['index','store']]);
         $this->middleware('permission:proyecto-create', ['only' => ['create','store']]);
         $this->middleware('permission:proyecto-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:proyecto-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Si es administrador redirecciona a la vista general de todos los proyectos del sistema
        if(Auth::user()->hasRole('Admin')){
            $proyectos = Proyecto::latest()->paginate(5);

            return view('proyectos.index', compact('proyectos'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
        }
        else{   //Si no es admin, retorna a la vista detalle de un departamento
            if(Auth::user()->departamento){
                return redirect()->route('departamento.proyectos.index', Auth::user()->departamento);
            }   //Si no tiene departamento retorna a una vista donde se le informa que no tiene departamento
            else{
                return view('sin_departamento');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $users = User::all();
        $puertos = Puerto::all();
        $departamentos = Departamento::all();
        return view('proyectos.create',compact('users','puertos','departamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:200',
            'descripcion' => 'required|max:255',
            'puerto_id' => 'required|exists:puertos,id',
            'user_id' => 'required|exists:users,id',
            'departamento_id' => 'required|exists:departamentos,id',
            'costo' => 'required|numeric',
            // 'contratado' => 'required|numeric',
        ]);
        $estado = Estado::firstOrCreate([
            'nombre' => 'Pendiente'
        ]);
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $monto_usd = $request->input('costo')/$dolarCompra;


        Proyecto::create(array_merge($request->all(), ['estado_id' => $estado->id, 'contratado'=>0, 'cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]));

        return redirect()->route('proyectos.index')
        ->with('success','Proyecto se ha creado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto)
    {
        return view('proyectos.detail', compact('proyecto'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyecto $proyecto)
    {
        $users = User::all();
        $estados = Estado::all();
        $puertos = Puerto::all();
        $departamentos = Departamento::all();
        return view('proyectos.edit', compact('proyecto','users','estados','puertos','departamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto)
    {
        $input = $request->validate([
            'nombre' => 'max:200',
            'descripcion' => 'max:200',
            'puerto_id' => 'exists:puertos,id',
            'user_id' => 'exists:users,id',
            'departamento_id' => 'exists:departamentos,id',
            'estado_id' => 'exists:estados,id',
            'costo' => 'numeric',
        ]);
        $dolar = $this->dolar();
        $dolarCompra = $dolar['dolarCompra'];
        $monto_usd = $request->input('costo')/$dolarCompra;
        $proyecto->fill(array_merge($input, ['cotizacion_dia'=>$dolarCompra, 'monto_usd'=>$monto_usd]))->save();
        return redirect()->route('proyectos.index')
        ->with('success','Proyecto se ha actualizado exitosamente.');
    }

    /**
     * Visualizar reporte de proyectos en pdf
     *
     * @return PDF
     */
    public function reporteVer()
    {
        $proyectos = Proyecto::all();
        $view = View::make('proyectos.reporte', compact('proyectos'))->render();
        $pdf = App::make('dompdf.wrapper');        
        $pdf->loadHTML($view);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->stream('proyectos.pdf'); 
    }

    /**
     * Descargar reporte de proyectos en pdf
     *
     * @return PDF
     */
    public function reporteDescargar()
    {
        $proyectos = Proyecto::all();
        $view = View::make('proyectos.reporte', compact('proyectos'))->render();
        $pdf = App::make('dompdf.wrapper');        
        $pdf->loadHTML($view);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('proyectos.pdf'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto)
    {
        $proyecto->delete();
        return redirect()->route('proyectos.index')
        ->with('success','El proyecto ha sido eliminado exitosamente.');
    }

    public function dolar(){
        $url ="https://dolar.melizeche.com/api/1.0/"; 
 
        $ch = \curl_init($url);

        // Configuring curl options 
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     
            CURLOPT_HTTPHEADER => array('Accept: application/json'), 
            CURLOPT_SSL_VERIFYPEER => false,     
        ); 
        // Setting curl options 
        \curl_setopt_array( $ch, $options );
        
        // Getting results 
        $response = curl_exec($ch); // Getting jSON result string   
        
        // Cerrar el recurso cURL y liberar recursos del sistema 
        \curl_close($ch);
        //print_r($response);
        
        $data = json_decode($response, true);     
        //var_dump($data);
        $fecha=$data['updated'];
        $date = strtotime($fecha);

        $MydcambiosCompra=$data['dolarpy']['mydcambios']['compra'];
        $MydcambiosVenta=$data['dolarpy']['mydcambios']['venta'];

        $resultado = array('dolarCompra'=>$MydcambiosCompra, 'dolarVenta'=> $MydcambiosVenta);
        return $resultado;
    }
}
