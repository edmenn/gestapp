<?php

namespace App\Http\Controllers;

use App\Models\Puerto;
use Illuminate\Http\Request;

class PuertoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $puertos = Puerto::orderBy('id','ASC')->paginate(10);
        return view('puertos.index',compact('puertos'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('puertos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'direccion' => 'required|max:255',
        ]);

        Puerto::create($request->all());

        return redirect()->route('puertos.index')
                        ->with('success','Puerto creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Puerto  $puerto
     * @return \Illuminate\Http\Response
     */
    public function show(Puerto $puerto)
    {
        return view('puertos.show', compact('puerto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Puerto  $puerto
     * @return \Illuminate\Http\Response
     */
    public function edit(Puerto $puerto)
    {
        return view('puertos.edit', compact('puerto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Puerto  $puerto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Puerto $puerto)
    {
        $input = $this->validate($request, [
            'nombre' => 'max:100',
            'direccion' => 'max:255',
        ]);

        $puerto->fill($input)->save();

        return redirect()->route('puertos.index')
                        ->with('success','Puerto se ha actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Puerto  $puerto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Puerto $puerto)
    {
        $puerto->delete();
        return redirect()->route('puertos.index')
        ->with('success','Proveedor ha sido eliminado exitosamente.');
    }
}
