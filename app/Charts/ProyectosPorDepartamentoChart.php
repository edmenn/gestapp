<?php

declare(strict_types = 1);

namespace App\Charts;
use Illuminate\Support\Facades\DB;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use App\Models\Proyecto;
use App\Models\Departamento;
use App\Models\Estado;
use App\Models\User;

//Estado de los Proyecto por Departamento
class ProyectosPorDepartamentoChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public ?string $name = 'proyectos_dep_chart';

    public ?string $routeName = 'proyectos_dep_chart';

    public function handler(Request $request): Chartisan
    {
        $departamento_id = $request->id;
        error_log("El id de request es: " . $departamento_id);
        $departamento = Departamento::findOrFail($departamento_id);
        error_log("El departamento es: " . $departamento);
        $estados = Estado::all();
        $labels = [];
        $count = [];
        error_log("Los estados son: " . $estados);
        foreach ($estados as $estado){
            array_push($labels,$estado->nombre);
            $estado_id = $estado->id;
            $proyectos = DB::table('proyectos')->where(function($query) use ($departamento_id, $estado_id) {
                if ( ! empty($departamento_id)) {
                    $query->where('departamento_id', '=', $departamento_id);
                }
                if ( ! empty($estado_id)) {
                    $query->where('estado_id', '=', $estado_id);
                }
            })->get();
            $cant_proyectos = $proyectos->count();
            error_log("La variable proyecto trae: " . $proyectos);
            array_push($count,$cant_proyectos);
            // array_push($count,$proyectos->count());
        }

        return Chartisan::build()
            ->labels($labels)
            ->dataset('Sample', $count);
    }
}