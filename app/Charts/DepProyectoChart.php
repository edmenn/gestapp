<?php

declare(strict_types = 1);

namespace App\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use App\Models\Departamento;
use App\Models\Proyecto;
use App\Models\Estado;
use App\Models\User;

//Cantidad de Proyecto por Departamento
class DepProyectoChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public ?string $name = 'dep_proyecto_chart';

    public ?string $routeName = 'dep_proyecto_chart';

    public function handler(Request $request): Chartisan
    {
        // return Chartisan::build()
        //     ->labels(['First', 'Second', 'Third'])
        //     ->dataset('Sample', [1, 2, 3])
        //     ->dataset('Sample 2', [3, 2, 1]);

        // $estados = DB::table('estados')->get();
        $departamentos = Departamento::all();
        $estados = Estado::all();
        $labels = [];
        $count = [];
        foreach ($departamentos as $departamento){
            array_push($labels,$departamento->nombre);
            array_push($count,$departamento->proyectos->count());
        }
        // $values = Estado::with('proyectos' )->get();
        // foreach ($values as $item) {
        //     array_push($count,$item->proyectos->count());
        // }
        return Chartisan::build()
            ->labels($labels)
            ->dataset('Sample', $count);
    }
}