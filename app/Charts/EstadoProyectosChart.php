<?php

declare(strict_types = 1);

namespace App\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use App\Models\Proyecto;
use App\Models\Estado;
use App\Models\User;

class EstadoProyectosChart extends BaseChart
{
    public ?string $name = 'my_chart';

    public ?string $routeName = 'my_chart';

    public function handler(Request $request): Chartisan
    {
        // return Chartisan::build()
        //     ->labels(['First', 'Second', 'Third'])
        //     ->dataset('Sample', [1, 2, 3])
        //     ->dataset('Sample 2', [3, 2, 1]);

        // $estados = DB::table('estados')->get();
        $estados = Estado::all();
        $labels = [];
        $count = [];
        foreach ($estados as $estado){
            array_push($labels,$estado->nombre);
            array_push($count,$estado->proyectos->count());
        }
        // $values = Estado::with('proyectos' )->get();
        // foreach ($values as $item) {
        //     array_push($count,$item->proyectos->count());
        // }
        return Chartisan::build()
            ->labels($labels)
            ->dataset('Sample', $count);
    }
}